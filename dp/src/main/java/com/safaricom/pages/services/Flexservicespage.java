package com.safaricom.pages.services;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class Flexservicespage {
	public AndroidDriver<MobileElement> driver;
  
  public Flexservicespage() {
	  
  }
  public Flexservicespage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_dp_toolbarTitle")
  public AndroidElement flex_title;

  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_flex_balance")
  public AndroidElement tv_flex_balance;
  
  @AndroidFindBy(xpath="//android.widget.RadioButton[@text='MONTHLY FLEX']")
  public AndroidElement monthlyflex_click;
  
  @AndroidFindBy(xpath="//android.widget.RadioButton[@text='WEEKLY FLEX']")
  public AndroidElement weeklyflex_click;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_buy_bundle")
  public AndroidElement buybundleClick;

  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_stop_autorenew")
  public AndroidElement tv_stop_autorenew;

  @AndroidFindBy(id="com.selfcare.safaricom:id/flex_buy_other")
  public AndroidElement flex_buy_other;
  
  
  
}
