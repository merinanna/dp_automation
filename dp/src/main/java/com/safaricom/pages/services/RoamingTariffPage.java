package com.safaricom.pages.services;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class RoamingTariffPage {
	public AndroidDriver<MobileElement> driver;
  
  public RoamingTariffPage() {
	  
  }
  public RoamingTariffPage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/roaming_traiff_title")
  public AndroidElement roaming_tariff_title;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/roaming_traiff_continent_spinner")
  public AndroidElement roaming_traiff_continent_spinner;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Australia']")
  public AndroidElement roaming_traiff_continent;
 
  @AndroidFindBy(id="com.selfcare.safaricom:id/roaming_traiff_country_spinner")
  public AndroidElement roaming_traiff_country_spinner;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Fiji']")
  public AndroidElement roaming_traiff_country;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_ok")
  public AndroidElement btn_ok ;

}
