package com.safaricom.pages.services;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class BongaServicesPage {
 public AndroidDriver<MobileElement> driver;
  
  public BongaServicesPage() {
	  
  }
  public BongaServicesPage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_dp_toolbarTitle")
  public AndroidElement bongaServices_title;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/bonga_services_title_tv")
  public AndroidElement title;
    
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_title_bonga_points")
  public AndroidElement bongapoints_title;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_date")
  public AndroidElement bonga_date;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_four_thousand")
  public AndroidElement bongapoint_value;
  
      
  //Redeem
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Redeem']")
  public AndroidElement redeemClick;
    
  //Transfer
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_bonga_transfer")
  public AndroidElement transferClick ;

  //View Redeemption History
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_redemption_history")
  public AndroidElement view_redemption_history_click;
    
  //Accumulation History
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_accumulation_history")
  public AndroidElement accumulation_history_Click ;
  
  }
