package com.safaricom.pages.services;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class SambazaservicesPage {
	public AndroidDriver<MobileElement> driver;
  
  public SambazaservicesPage() {
	  
  }
  public SambazaservicesPage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_dp_toolbarTitle")
  public AndroidElement sambazaservices_title;
 
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_bonga_points_title")
  public AndroidElement airtime_tv_bonga_points_title;
   
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_date_airtime")
  public AndroidElement tv_date_airtime;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_amount_airtime")
  public AndroidElement tv_amount_airtime;
     
  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_airtime_sambaza")
  public AndroidElement airtimesambazaClick;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_bonga_points_")
  public AndroidElement tv_bonga_points_data;
   
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_date_data")
  public AndroidElement tv_date_data;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_amount_data")
  public AndroidElement tv_amount_data;
    
  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_data_sambaza")
public AndroidElement datasambazaClick ;
}
