package com.safaricom.pages.services;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class RedemptionHistoryPage {
 public AndroidDriver<MobileElement> driver;
  
  public RedemptionHistoryPage() {
	  
  }
  public RedemptionHistoryPage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_dialog_title")
  public AndroidElement txt_dialog_title;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/text_date1")
  public AndroidElement text_date1;
    
  @AndroidFindBy(id="com.selfcare.safaricom:id/text_date2")
  public AndroidElement text_date2;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_date")
  public AndroidElement bonga_date;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_requestforbill")
  public AndroidElement tv_requestforbill;
    
  }
