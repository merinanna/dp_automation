package com.safaricom.pages.services;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class FlexBuyOtherPage {
	public AndroidDriver<MobileElement> driver;
  
  public FlexBuyOtherPage() {
	  
  }
  public FlexBuyOtherPage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
  public AndroidElement buyforothernumber_title;

  @AndroidFindBy(id="com.selfcare.safaricom:id/edt_mobile")
  public AndroidElement edt_mobile;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/spin_flex_bundle")
  public AndroidElement spin_flex_bundle;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='700 Flex @ KSH 599']")
  public AndroidElement flexbundle_select;
  
  @AndroidFindBy(xpath="//android.widget.RadioButton[@text='Airtime']")
  public AndroidElement airtime_click;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_cancel")
  public AndroidElement btn_cancel;

  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_continue")
  public AndroidElement btn_continue;

  
}
