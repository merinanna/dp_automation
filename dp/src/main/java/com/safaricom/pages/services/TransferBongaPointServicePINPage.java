package com.safaricom.pages.services;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class TransferBongaPointServicePINPage {
 public AndroidDriver<MobileElement> driver;
  
  public TransferBongaPointServicePINPage() {
	  
  }
  public TransferBongaPointServicePINPage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/text_heading")
  public AndroidElement text_heading;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/sub_heading")
  public AndroidElement sub_heading ;

  @AndroidFindBy(id="com.selfcare.safaricom:id/et_pin")
  public AndroidElement enter_pin;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_forgot_pin")
  public AndroidElement btn_forgot_pin ;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_cancel")
  public AndroidElement btn_cancel;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_ok")
  public AndroidElement btn_ok ;
  
  }
