package com.safaricom.pages.menu;

import java.util.List;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class Menudrawer {
	public AndroidDriver<MobileElement> driver;
  
  public Menudrawer() {
	  
  }
  public Menudrawer(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @FindBy(how = How.ID,using="com.selfcare.safaricom:id/tv_title")
	public  List<MobileElement> L1;
  
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='MY ACCOUNT']")
  public AndroidElement myAccount_click;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='SERVICES']")
  public AndroidElement services_click;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='MY BILL']")
  public AndroidElement myBill_click;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='DATA USAGE']")
  public AndroidElement dataUsage_click;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='KNOWLEDGE BASE']")
  public AndroidElement knowledgebase_click;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='STORE LOCATOR']")
  public AndroidElement storelocator_click;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='TELL A FRIEND']")
  public AndroidElement tellafriend;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='FEEDBACK & RATING']")
  public AndroidElement feedbacknrating_click;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='CONTACT US']")
  public AndroidElement contactus_click;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='SETTINGS']")
  public AndroidElement settings_clicks;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='BACK TO SAFARICOM APP']")
  public AndroidElement backtosafaricom_click ;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='LOGOUT']")
  public AndroidElement logout_click;
  
  
}
