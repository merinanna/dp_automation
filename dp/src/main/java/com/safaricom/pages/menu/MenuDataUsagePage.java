package com.safaricom.pages.menu;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MenuDataUsagePage {
	public AndroidDriver<MobileElement> driver;
  
  public MenuDataUsagePage() {
	  
  }
  public MenuDataUsagePage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_dp_toolbarTitle")
  public AndroidElement dataUsage_title; 
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/startDate")
  public AndroidElement startDate;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/endDate")
  public AndroidElement endDate;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/totalDataUsed")
  public AndroidElement totalDataUsed;
  
       
  }
