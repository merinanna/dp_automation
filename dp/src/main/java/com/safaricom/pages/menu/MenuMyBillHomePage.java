package com.safaricom.pages.menu;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MenuMyBillHomePage {

  public AndroidDriver<MobileElement> driver;
  
  public MenuMyBillHomePage() {
	  
  }
  public MenuMyBillHomePage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
  public AndroidElement myBill_title;
   
  //My Bill
 
  @AndroidFindBy(id="com.selfcare.safaricom:id/labelMyBill")
  public AndroidElement myBill_label;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_bill_usage_balance")
  public AndroidElement txt_bill_usage_balance;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_bill_prepayment")
  public AndroidElement txt_bill_prepayment;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_bill_credit_limit")
  public AndroidElement txt_bill_credit_limit;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_requestforbill")
  public AndroidElement tv_requestforbill;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/buttonPayTheBill")
  public AndroidElement buttonPayTheBill;
  
  
  //Account Balance
 
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
  public AndroidElement accountBalance_label;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_usage_account_asat")
  public AndroidElement txt_usage_account_asat;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_voice_balance")
  public AndroidElement txt_voice_balance;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_data_balance")
  public AndroidElement txt_data_balance;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_sms_balance")
  public AndroidElement txt_sms_balance;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_redeem_bonga")
  public AndroidElement btn_redeem_bonga;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_buy_data")
  public AndroidElement btn_buy_data;
  
  //Prepaid Usage Statement
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/labelUsageStatementTitle")
  public AndroidElement prepadiUsageStatement_title;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/buttonFullStatement")
  public AndroidElement buttonFullStatement;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/buttonMiniStatement")
  public AndroidElement buttonMiniStatement;
  
  


  
  
}
