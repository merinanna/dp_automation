package com.safaricom.pages.menu;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class Menu_services {
	public AndroidDriver<MobileElement> driver;
  
  public Menu_services() {
	  
  }
  public Menu_services(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_dp_toolbarTitle")
  public AndroidElement services_title;
    
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Bonga Services']")
  public AndroidElement bongaservice_click;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='FLEX']")
  public AndroidElement flexServices_click;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Data & SMS Plans']")
  public AndroidElement datansmsplans_click;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='My SMS Services']")
  public AndroidElement mysmsservices_click;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Sambaza Services']")
  public AndroidElement sambazaservices_click;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Skiza Services']")
  public AndroidElement skizaservices_click;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Roaming Services']")
  public AndroidElement roamingservices_click;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Okoa Services']")
  public AndroidElement okoaservices_click;
  
}
