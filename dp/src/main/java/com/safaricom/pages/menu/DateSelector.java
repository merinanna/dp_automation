package com.safaricom.pages.menu;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class DateSelector {
	public AndroidDriver<MobileElement> driver;
  
  public DateSelector() {
	  
  }
  public DateSelector(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="android:id/date_picker_header_year")
  public AndroidElement date_picker_header_year; 
  
  @AndroidFindBy(id="android:id/button1")
  public AndroidElement ok_button;
  
  @AndroidFindBy(xpath="//android.widget.Button[@text='Cancel']")
  public AndroidElement cancel_button;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/startDate")
  public AndroidElement startDate;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/endDate")
  public AndroidElement endDate;
       


}

