package com.safaricom.pages.menu;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class Menu_setting {
	public AndroidDriver<MobileElement> driver;
  
  public Menu_setting() {
	  
  }
  public Menu_setting(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
  public AndroidElement settings_title;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/buttonSetServicePin")
  public AndroidElement changeservicepin_click;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/buttonSecureWithPin")
  public AndroidElement securewithpin_clicks;
  
}
