package com.safaricom.pages.mpesa;


import java.util.List;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MinistatementHomePage {
 
	public AndroidDriver<MobileElement> driver;
	public MinistatementHomePage()
	{
		
	}
	
	public MinistatementHomePage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	  // Ministatement Home Page

	
	@FindBy(how= How.ID,using="com.selfcare.safaricom:id/loan_title")
	public List<MobileElement> L1;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_dp_toolbarTitle")
	  public AndroidElement ministatement_title;
	
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='Deposit Mini Statement']")
	  public AndroidElement depositministatement_click;
	
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='Loan Mini Statement")
	  public AndroidElement loanministatement_click;
	  
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='Lock Savings Account Mini Statement']")
	  public AndroidElement locksavingsaccountministatement_click;
	  
	
	}
