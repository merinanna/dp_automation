package com.safaricom.pages.mpesa;

import java.util.List;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class BuyAirtimeMpesaPinPage {
 
	public AndroidDriver<MobileElement>  driver;
	public BuyAirtimeMpesaPinPage()
	{
		
	}
	
	public BuyAirtimeMpesaPinPage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	 @FindBy(how = How.CLASS_NAME,using="android.widget.TextView")
     public  List<MobileElement> L1;
	 
	 @FindBy(how = How.CLASS_NAME,using="android.widget.ImageButton")
     public  List<MobileElement> L2;
	 
	  
	 @AndroidFindBy(xpath="//android.widget.TextView[@text='BUY AIRTIME']")
	  public AndroidElement buyairtime_title ;
	 
	 @AndroidFindBy(xpath="//android.widget.TextView[@text='1']")
	  public AndroidElement one_btn ;
	 
	 @AndroidFindBy(xpath="//android.widget.TextView[@text='2']")
	  public AndroidElement two_btn ;
	 
	 @AndroidFindBy(xpath="//android.widget.TextView[@text='3']")
	  public AndroidElement three_btn ;
	 
	 @AndroidFindBy(xpath="//android.widget.TextView[@text='4']")
	  public AndroidElement four_btn ;
	 
	 @AndroidFindBy(xpath="//android.widget.TextView[@text='5']")
	  public AndroidElement five_btn ;
	 
	 @AndroidFindBy(xpath="//android.widget.TextView[@text='6']")
	  public AndroidElement six_btn ;
	 
	 @AndroidFindBy(xpath="//android.widget.TextView[@text='7']")
	  public AndroidElement seven_btn ;
	 
	 @AndroidFindBy(xpath="//android.widget.TextView[@text='8']")
	  public AndroidElement eight_btn ;
	 
	 @AndroidFindBy(xpath="//android.widget.TextView[@text='9']")
	  public AndroidElement nine_btn ;
	 
	 @AndroidFindBy(xpath="//android.widget.TextView[@text='0']")
	  public AndroidElement zero_btn ;
	 
	 @AndroidFindBy(xpath="//android.widget.TextView[@text='OK']")
	  public AndroidElement ok_btn ;
	 
	  @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_clear")
	  public AndroidElement btn_clear;
	  	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/pin_code_button_ok")
	  public AndroidElement btn_ok;
	
}
