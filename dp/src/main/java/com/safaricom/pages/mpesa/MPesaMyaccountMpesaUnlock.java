package com.safaricom.pages.mpesa;


import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MPesaMyaccountMpesaUnlock {
 
	public AndroidDriver<MobileElement> driver;
	public MPesaMyaccountMpesaUnlock()
	{
		
	}
	
	public MPesaMyaccountMpesaUnlock(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	//My Account Home Page
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/labelTitle")
	public AndroidElement mpesaunlock_labelTitle; //UNLOCK YOUR M-PESA ACCOUNT
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/inputPin")
	public AndroidElement inputPin;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/buttonPositive")
	public AndroidElement buttonPositive;
	
	
	
	}
