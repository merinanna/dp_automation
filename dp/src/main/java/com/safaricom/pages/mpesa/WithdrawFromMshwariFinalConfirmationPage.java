package com.safaricom.pages.mpesa;


import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class WithdrawFromMshwariFinalConfirmationPage {
 
	public AndroidDriver<MobileElement> driver;
	public WithdrawFromMshwariFinalConfirmationPage()
	{
		
	}
	
	public WithdrawFromMshwariFinalConfirmationPage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	  @AndroidFindBy(id="com.selfcare.safaricom:id/labelMessage")
	  public AndroidElement labelMessage;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/button")
	  public AndroidElement ok_button;
	  	  
	
	
	}
