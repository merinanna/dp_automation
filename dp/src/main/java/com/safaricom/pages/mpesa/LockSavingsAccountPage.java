package com.safaricom.pages.mpesa;


import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class LockSavingsAccountPage {
 
	public AndroidDriver<MobileElement> driver;
	public LockSavingsAccountPage()
	{
		
	}
	
	public LockSavingsAccountPage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	//Lock savings account  Home Page
	
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='Open Lock Account']")
	  public AndroidElement openlockaccount_click;
	
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='Save']")
	  public AndroidElement save_click;
	  
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='Withdraw']")
	  public AndroidElement withdraw_click;
	  
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='Mini Statement']")
	  public AndroidElement ministatement_click;
	  
	  
	  
	  //Savings 
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	  public AndroidElement locksavingsaccount_title;//LOCK SAVINGS ACCOUNT
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/textSavings")
	  public AndroidElement locksavingsaccount_textSavings;
	  	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/textLoanAmt")
	  public AndroidElement locksavingsaccount_textLoanAmt;
	  
	   @AndroidFindBy(id="com.selfcare.safaricom:id/textDueDate")
	  public AndroidElement locksavingsaccount_textDueDate;
	  
	   @AndroidFindBy(id="com.selfcare.safaricom:id/textLoanLimit")
	  public AndroidElement locksavingsaccount_textLoanLimit;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/textTargetAmt")
	  public AndroidElement locksavingsaccount_textTargetAmt;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/textMaturityDate")
	  public AndroidElement locksavingsaccount_textMaturityDate;
	
	
	}
