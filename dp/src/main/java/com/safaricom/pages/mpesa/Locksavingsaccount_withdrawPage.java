package com.safaricom.pages.mpesa;


import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class Locksavingsaccount_withdrawPage {
 
	public AndroidDriver<MobileElement> driver;
	public Locksavingsaccount_withdrawPage()
	{
		
	}
	
	public Locksavingsaccount_withdrawPage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	  //Withdraw
	
		 @AndroidFindBy(id="com.selfcare.safaricom:id/title_lock_ac")
		  public AndroidElement withdraw_title;
		  		  	  
		  @AndroidFindBy(id="com.selfcare.safaricom:id/et_amount")
		  public AndroidElement withdraw_et_amount;
		  		  
		  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_continue")
		  public AndroidElement btn_continue;
	
	
	}
