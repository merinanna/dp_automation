package com.safaricom.pages.mpesa;


import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class LockSavingsAccount_SavePage {
 
	public AndroidDriver<MobileElement> driver;
	public LockSavingsAccount_SavePage()
	{
		
	}
	
	public LockSavingsAccount_SavePage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	  //Save
	
	 @AndroidFindBy(id="com.selfcare.safaricom:id/title_lock_ac")
	  public AndroidElement save_title;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/text")
	  public AndroidElement save_acc_from_click;
	  
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='M-PESA']")
	  public AndroidElement mpesa_click;
	  
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='M-Shwari']")
	  public AndroidElement mshwari_click;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/et_amount")
	  public AndroidElement save_et_amount;
	  
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_continue")
	  public AndroidElement btn_continue;
	 
	  
	
	
	}
