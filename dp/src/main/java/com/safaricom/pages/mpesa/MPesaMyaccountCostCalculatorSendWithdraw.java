package com.safaricom.pages.mpesa;


import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MPesaMyaccountCostCalculatorSendWithdraw {
 
	public AndroidDriver<MobileElement> driver;
	public MPesaMyaccountCostCalculatorSendWithdraw()
	{
		
	}
	
	public MPesaMyaccountCostCalculatorSendWithdraw(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	//My Account Home Page
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/edt_send_withdraw_amount")
	public AndroidElement edt_send_withdraw_amount; 
		
	
	}
