package com.safaricom.pages.mpesa;


import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MPesaMyaccountCostCalculatorHomePage {
 
	public AndroidDriver<MobileElement> driver;
	public MPesaMyaccountCostCalculatorHomePage()
	{
		
	}
	
	public MPesaMyaccountCostCalculatorHomePage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	//My Account Home Page
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_dp_toolbarTitle")
	public AndroidElement costcalculator_title; //COST CALCULATOR
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/radioSendWithdraw")
	public AndroidElement tvsendWithdrawLabel;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/radioLipaNaMPesa")
	public AndroidElement tvMpesaLabel;
	
	
	
	}
