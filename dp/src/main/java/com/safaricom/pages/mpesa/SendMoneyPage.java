package com.safaricom.pages.mpesa;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class SendMoneyPage {
 
	public AndroidDriver<MobileElement>  driver;
	public SendMoneyPage()
	{
		
	}
	
	public SendMoneyPage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_dp_toolbarTitle")
	public AndroidElement SendMoney_title;
	
	
	//<-------------------------Send to One-------------------------------->
	@AndroidFindBy(id="com.selfcare.safaricom:id/radioSendToOne")
	public AndroidElement SendtoOneClick;
	@AndroidFindBy(id="com.selfcare.safaricom:id/inputSendToOneNumber")
	public AndroidElement SendtoOnePhoneNumber;
	@AndroidFindBy(id="com.selfcare.safaricom:id/inputSendToOneAmount")
	public AndroidElement SendtoOnePhoneAmount;
	@AndroidFindBy(id="com.selfcare.safaricom:id/buttonSendToOneContinue")
	public AndroidElement SendtoOneContinue;
		
	
	//<-------------------------Send to Other-------------------------------->
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/radioSendToOther")
	public AndroidElement SendtoOtherClick;
	@AndroidFindBy(id="com.selfcare.safaricom:id/inputSendToOtherNumber")
	public AndroidElement SendtoOtherPhoneNumber;
	@AndroidFindBy(id="com.selfcare.safaricom:id/inputSendToOtherAmount")
	public AndroidElement SendtoOtherPhoneAmount;
	@AndroidFindBy(id="com.selfcare.safaricom:id/buttonSendToOtherContinue")
	public AndroidElement SendToOtherContinue;
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_ok")
	public AndroidElement errormsg_close;	
	@AndroidFindBy(className="android.widget.ImageButton")
	public AndroidElement backclick;
	
	
	
}
