package com.safaricom.pages.mpesa;


import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class Ministatement_LoanHomePage {
 
	public AndroidDriver<MobileElement> driver;
	public Ministatement_LoanHomePage()
	{
		
	}
	
	public Ministatement_LoanHomePage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	

	  // Loan Ministatement Home Page

	  @AndroidFindBy(id="com.selfcare.safaricom:id/tvStmtHeader")
	  public AndroidElement loanministatement_header;  //Loan Mini Statement;
	  
	
	}
