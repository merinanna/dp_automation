package com.safaricom.pages.mpesa;


import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class SendtoMshwariPage {
 
	public AndroidDriver<MobileElement> driver;
	public SendtoMshwariPage()
	{
		
	}
	
	public SendtoMshwariPage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_dp_toolbarTitle")
	  public AndroidElement main_title;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/title_mshwari")
	  public AndroidElement sendtomshwari_title;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/et_mshwari_amount")
	  public AndroidElement et_mshwari_amount;
	  
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_continue")
	  public AndroidElement btn_continue;
	  
	  
	
	
	}
