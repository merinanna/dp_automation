package com.safaricom.pages.mpesa;

import java.util.List;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class WithdrawCashErrorPage {
 
	public AndroidDriver<MobileElement>  driver;
	public WithdrawCashErrorPage()
	{
		
	}
	
	public WithdrawCashErrorPage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	  @FindBy(how = How.ID,using="com.selfcare.safaricom:id/labelValue")
      public  List<MobileElement> L1;
	 
	  @AndroidFindBy(id="com.selfcare.safaricom:id/img_success_failure")
	  public AndroidElement failure_image;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/txtMessage")
	  public AndroidElement txt_message;
	  
	 @AndroidFindBy(id="com.selfcare.safaricom:id/buttonNegative")
	  public AndroidElement reverse_button;
	
	  @AndroidFindBy(id="com.selfcare.safaricom:id/buttonPositive")
	  public AndroidElement done_button;
	
}
