package com.safaricom.pages.mpesa;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class WithdrawCashPage {
 
	public AndroidDriver<MobileElement> driver;
	public WithdrawCashPage()
	{
		
	}
	
	public WithdrawCashPage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_dp_toolbarTitle")
	public AndroidElement withdrawcash_title;
	
	//<-------------------------Withdraw From Agent-------------------------------->
		
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/radioAgent")
	public AndroidElement withdrawFromAgent_click;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/inputAgentNumber")
	public AndroidElement edt_mobilenumber_WFAgent;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/inputAmount")
	public AndroidElement et_amount_WFAgent;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/buttonNext")
	public AndroidElement WFAgentContinueClick;
			
		
	//<-------------------------Withdraw From ATM-------------------------------->
		
	@AndroidFindBy(id="com.selfcare.safaricom:id/radioATM")
	public AndroidElement withdrawFromATM_click;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/inputAgentNumber")
	public AndroidElement edt_agentnumber_WFATM;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/buttonNext")
	public AndroidElement WFATMContinueClick;
		
		
//	@FindBy(how = How.ID,using="com.selfcare.safaricom:id/iv_expand_ico")
//	public  List<MobileElement> L1;
	

	
	
}
