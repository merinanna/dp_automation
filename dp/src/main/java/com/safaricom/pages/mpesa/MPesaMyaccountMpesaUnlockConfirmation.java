package com.safaricom.pages.mpesa;


import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MPesaMyaccountMpesaUnlockConfirmation {
 
	public AndroidDriver<MobileElement> driver;
	public MPesaMyaccountMpesaUnlockConfirmation()
	{
		
	}
	
	public MPesaMyaccountMpesaUnlockConfirmation(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	//My Account Home Page
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/labelMessage")
	public AndroidElement mpesaunlock_labelMessage; //UNLOCK YOUR M-PESA ACCOUNT
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/button")
	public AndroidElement ok_button;
	
		
	}
