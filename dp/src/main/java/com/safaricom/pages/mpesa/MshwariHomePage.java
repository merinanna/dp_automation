package com.safaricom.pages.mpesa;

import java.util.List;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MshwariHomePage {
 
	public AndroidDriver<MobileElement> driver;
	public MshwariHomePage()
	{
		
	}
	
	public MshwariHomePage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	
	@FindBy(how = How.ID,using="com.selfcare.safaricom:id/mpesa_title")
	public List<MobileElement> L1;
	
	
	
	//M-Shwari Home Page
	
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='SEND TO M-SHWARI']")
	  public AndroidElement sendtomshwari_click;
	
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='WITHDRAW FROM\n" + 
	  		"M-SHWARI']")
	  public AndroidElement withdrawfrommshwari_click;
	  
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='LOCK SAVINGS ACCOUNT']")
	  public AndroidElement locksavingsaccount_click;
	  
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='LOAN']")
	  public AndroidElement loan_click;
	 	  
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='MINI STATEMENT']")
	  public AndroidElement ministatement_click;
	  
	  
	  //Savings 
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	  public AndroidElement mshwari_title;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/textSavings")
	  public AndroidElement textSavings;
	  	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/textLoanAmt")
	  public AndroidElement textLoanAmt;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/textDueDate")
	  public AndroidElement textDueDate;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/textLoanLimit")
	  public AndroidElement textLoanLimit;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/textTargetAmt")
	  public AndroidElement textTargetAmt;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/textMaturityDate")
	  public AndroidElement textMaturityDate;
	
	
	}
