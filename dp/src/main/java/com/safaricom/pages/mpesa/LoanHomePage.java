package com.safaricom.pages.mpesa;


import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class LoanHomePage {
 
	public AndroidDriver<MobileElement> driver;
	public LoanHomePage()
	{
		
	}
	
	public LoanHomePage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	  // Loan Home Page
	
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='Request Loan']")
	  public AndroidElement requestLoan_click;
	
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='Pay Loan']")
	  public AndroidElement payLoan_click;
	  
	 	  
	  
	  // Savings 
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	  public AndroidElement loan_title;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/textSavings")
	  public AndroidElement loan_textSavings;
	  	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/textLoanAmt")
	  public AndroidElement loan_textLoanAmt;
	  	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/textDueDate")
	  public AndroidElement loan_textDueDate;
	  	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/textLoanLimit")
	  public AndroidElement loan_textLoanLimit;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/textTargetAmt")
	  public AndroidElement loan_textTargetAmt;
	  
	  @AndroidFindBy(id="com.selfcare.safaricom:id/textMaturityDate")
	  public AndroidElement loan_textMaturityDate;
	
	
	}
