package com.safaricom.pages.mpesa;


import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class BuyGoodsandServicesPage {
 
	public AndroidDriver<MobileElement> driver;
	public BuyGoodsandServicesPage()
	{
		
	}
	
	public BuyGoodsandServicesPage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	//Buy Goods
		
	@AndroidFindBy(id="com.selfcare.safaricom:id/radio_buy_goods")
	public AndroidElement BuygoodsClick;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/edt_bgs_till_no")
	public AndroidElement Buygoods_edt_till_no;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/edt_bgs_amount")
	public AndroidElement Buygoods_edt_till_amount;
				
	@AndroidFindBy(id="com.selfcare.safaricom:id/button_bgs_next")
	public AndroidElement Buygoods_continue;
	
	}
