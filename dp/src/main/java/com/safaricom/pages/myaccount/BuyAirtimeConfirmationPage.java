package com.safaricom.pages.myaccount;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class BuyAirtimeConfirmationPage {
 
	public AndroidDriver<MobileElement> driver;
	public BuyAirtimeConfirmationPage()
	{
		
	}
	
	public BuyAirtimeConfirmationPage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	

	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_dialog_title")
	public AndroidElement confirmation_title;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_dialog_account_number")
	public AndroidElement txt_dialog_account_number;
		
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_dialog_amount")
	public AndroidElement txt_dialog_amount;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_mpesa_sendto_value")
	public AndroidElement tv_mpesa_sendto_value;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_continue_dilaog")
	public AndroidElement txt_continue_dilaog;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_cancel_dilaog")
	public AndroidElement txt_cancel_dilaog;
	
	
	
}
