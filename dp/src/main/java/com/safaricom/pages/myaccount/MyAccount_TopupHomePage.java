package com.safaricom.pages.myaccount;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MyAccount_TopupHomePage {

  public AndroidDriver<MobileElement> driver;
  
  public MyAccount_TopupHomePage() {
	  
  }
  public MyAccount_TopupHomePage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
  public AndroidElement txt_title;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_dp_toolbarTitle")
  public AndroidElement buyairtime_txt_title;
      
  @AndroidFindBy(id="com.selfcare.safaricom:id/labelTopUp")
  public AndroidElement labelTopUp;
    
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_topup_asat")
  public AndroidElement txt_topup_asat ;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_airtime_balance")
  public AndroidElement tv_airtime_balance ;
    
  //My Number 
  @AndroidFindBy(xpath="//android.widget.RadioButton[@text='My Number']")
  public AndroidElement topup_my_number_click ;

  
  @AndroidFindBy(id="com.selfcare.safaricom:id/et_pin")
  public AndroidElement topup_et_pin;
    

  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_topup")
  public AndroidElement topup_mynum_send ;
  
  //Other Number 
  @AndroidFindBy(xpath="//android.widget.RadioButton[@text='Other Number']")
  public AndroidElement topup_other_number_click;
   
  @AndroidFindBy(id="com.selfcare.safaricom:id/edt_mobilenumber")
  public AndroidElement topup_edt_mobilenumber;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/et_pin")
  public AndroidElement topup_othernumber_et_pin;
 
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_topup")
  public AndroidElement topup_othernum_send ;
  
  //M-Pesa TopUp
  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_m_pesa_topup")
  public AndroidElement btn_m_pesa_topup_click ;
  
  //Redeem Bonga
  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_redeem_bonga")
  public AndroidElement btn_redeem_bonga_click ;
  
  
}
