package com.safaricom.pages.myaccount;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class BuyAirtimePage {
 
	public AndroidDriver<MobileElement> driver;
	public BuyAirtimePage()
	{
		
	}
	
	public BuyAirtimePage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_dp_toolbarTitle")
	public AndroidElement buyAirtime_title;
	
	
	//Self
	
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/radioSelf")
	public AndroidElement selfClick;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/inputAmount")
	public AndroidElement self_amount;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/buttonNext")
	public AndroidElement self_continue;
		
	//Other 
		
	@AndroidFindBy(id="com.selfcare.safaricom:id/radioOther")
	public AndroidElement otherClick;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/inputPhoneNumber")
	public AndroidElement inputPhoneNumber;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/inputAmount")
	public AndroidElement inputAmount;
				
	@AndroidFindBy(id="com.selfcare.safaricom:id/buttonNext")
	public AndroidElement other_continue;
	
	}
