package com.safaricom.pages.dphome;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class dealHomePage {
	public AndroidDriver<MobileElement> driver;
  
  public dealHomePage() {
	  
  }
  public dealHomePage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/labelTitle")
  public AndroidElement deal_title;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/imageBanner")
  public AndroidElement deal_imagebanner;
}
