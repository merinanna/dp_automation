package com.safaricom.pages.dphome;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class AccountHomePage {

  public AndroidDriver<MobileElement> driver;
  
  public AccountHomePage() {
	  
  }
  public AccountHomePage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/labelTitle")
  public AndroidElement account_label;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Data']")
  public AndroidElement data_balance_view ;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_amount")
  public AndroidElement data_limited_amount ;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_date")
  public AndroidElement data_expirydate ;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='SMS']")
  public AndroidElement sms_balance_view;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_amount")
  public AndroidElement sms_amount ;
 
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Voice']")
  public AndroidElement voice_balance_view ;
   
   @AndroidFindBy(id="com.selfcare.safaricom:id/txt_amount")
   public AndroidElement onNettalktime ;
   
   @AndroidFindBy(id="com.selfcare.safaricom:id/txt_amount")
   public AndroidElement offNettalktime ;
     
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Airtime']")
  public AndroidElement airtime_balance_view ;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_amount")
  public AndroidElement airtime_prepaid_balance ;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_date")
  public AndroidElement airtime_expirydate ;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Bonga']")
  public AndroidElement bonga_balance_view;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_amount")
  public AndroidElement bonga_balance_amount ;
 
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Okoa']")
  public AndroidElement okoa_balance_view;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_amount")
  public AndroidElement okoa_airtime_balance ;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_amount")
  public AndroidElement okoa_data_balance ;
  
}
