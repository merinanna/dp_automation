package com.safaricom.pages.dphome;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class dpHomePage {
	public AndroidDriver<MobileElement> driver;
  
  public dpHomePage() {
	  
  }
  public dpHomePage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
 
  @AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
  public AndroidElement title;
  
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/iv_menu_expand")
  public AndroidElement menu_expand;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/iv_profile_image")
  public AndroidElement iv_profile_image;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_greetings")
  public AndroidElement greeting_msg;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/labelFirstName")
  public AndroidElement FirstName;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_balance")
  public AndroidElement plan_balance;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/notify_frame_home")
  public AndroidElement notification_click;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/buttonDeals")
  public AndroidElement deals_click;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/buttonActiveBills")
  public AndroidElement active_bills;

  @AndroidFindBy(id="com.selfcare.safaricom:id/buttonMyBalance")
  public AndroidElement account_click;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='BUY PLAN']")
  public AndroidElement buy_plan;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_mybills")
  public AndroidElement my_bills;

  @AndroidFindBy(id="com.selfcare.safaricom:id/labelTitle")
  public AndroidElement paybill_labelTitle;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/buttonPay")
  public AndroidElement pay_button;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/buttonMpesa")
  public AndroidElement mpesa_click;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/buttonSendMoney")
  public AndroidElement sendMoney_click;
    
  @AndroidFindBy(id="com.selfcare.safaricom:id/buttonBuyGoods")
  public AndroidElement buyGoods_click;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/buttonMyDataUsage")
  public AndroidElement myDataUsage_click;
       
}
