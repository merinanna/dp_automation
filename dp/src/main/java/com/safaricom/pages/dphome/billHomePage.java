package com.safaricom.pages.dphome;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class billHomePage {
	public AndroidDriver<MobileElement> driver;
  
  public billHomePage() {
	  
  }
  public billHomePage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/buttonActiveBills")
  public AndroidElement billicon;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/buttonPay")
  public AndroidElement pay_button;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/buttonBillManager")
  public AndroidElement BillManagerbutton;
}
