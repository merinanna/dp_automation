package com.safaricom.pages.dphome;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MpesaHomePage {
	public AndroidDriver<MobileElement> driver;
  
  public MpesaHomePage() {
	  
  }
  public MpesaHomePage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
  public AndroidElement mpesa_title;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/show_balance")
  public AndroidElement mpesa_show_balance;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='M-PESA Statement']")
  public AndroidElement mpesa_statement;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/buttonScanQR")
  public AndroidElement mpesa_buttonScanQR;
   
  @AndroidFindBy(xpath="//android.widget.TextView[@text='SEND MONEY']")
  public AndroidElement sendMoneyClick;
    
  @AndroidFindBy(xpath="//android.widget.TextView[@text='WITHDRAW']")
  public AndroidElement withdraCashClick ;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='BUY AIRTIME']")
  public AndroidElement buyairtimeClick;
    
  @AndroidFindBy(xpath="//android.widget.TextView[@text='LIPA NA M-PESA']")
  public AndroidElement lipanampesaClick ;

  @AndroidFindBy(xpath="//android.widget.TextView[@text='LOANS & SAVINGS']")
  public AndroidElement loansandsavingsClick ;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='MY ACCOUNT']")
  public AndroidElement myaccountClick;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='FULIZA M-PESA']")
  public AndroidElement fulizampesaClick ;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='M-PESA GLOBAL']")
  public AndroidElement mpesaglobalClick;
 
}
