package com.safaricom.pages.MyBill;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class RequestForBillPage {
	public AndroidDriver<MobileElement> driver;
  
  public RequestForBillPage() {
	  
  }
  public RequestForBillPage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_dialog_title")
  public AndroidElement txt_dialog_title;
   
  @AndroidFindBy(id="com.selfcare.safaricom:id/edt_alert_edit")
  public AndroidElement email_address;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/et_pin")
  public AndroidElement service_pin;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_forgot_pin")
  public AndroidElement forget_pin;
    
  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_cancel")
  public AndroidElement btn_cancel;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_ok")
  public AndroidElement btn_ok;
  
}
