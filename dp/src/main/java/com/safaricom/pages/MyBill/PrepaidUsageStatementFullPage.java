package com.safaricom.pages.MyBill;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class PrepaidUsageStatementFullPage {
	public AndroidDriver<MobileElement> driver;
  
  public PrepaidUsageStatementFullPage() {
	  
  }
  public PrepaidUsageStatementFullPage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/labelTitle")
  public AndroidElement prepaidUsageStatement_title;
    
  @AndroidFindBy(id="com.selfcare.safaricom:id/inputEmail")
  public AndroidElement inputEmail;
    
  @AndroidFindBy(id="com.selfcare.safaricom:id/labelFromDate")
  public AndroidElement labelFromDate ;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/labelToDate")
  public AndroidElement labelToDate ;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/buttonPositive")
  public AndroidElement buttonPositive ;

}
