package com.safaricom.pages.bills;


import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class BillManagerPage {
 
	public AndroidDriver<MobileElement> driver;
	public BillManagerPage()
	{
		
	}
	
	public BillManagerPage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
	public AndroidElement billmanager_title;
	
	
	//Manage Bill
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='MANAGE BILLS']")
	public AndroidElement managebilltab_Click;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/imgExpand")
	public AndroidElement managebill_imgExpand;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/etAmount")
	public AndroidElement amount_field;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tvPay")
	public AndroidElement managebill_paybutton;
	
	//Frequently Paid
	@AndroidFindBy(xpath="//android.widget.TextView[@text='FREQUENTLY PAID']")
	public AndroidElement frequentlypiadtab_Click;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/inputSearchText")
	public AndroidElement searchfield;
	
	
	//Popular bill
	
	@AndroidFindBy(xpath="//android.widget.TextView[@text='POPULAR BILLS']")
	public AndroidElement popularbilltab_Click;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/ivBill")
	public AndroidElement popularbill_ivBill;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/etAccountNo")
	public AndroidElement popularbill_etAccountNo;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/buttonPositive")
	public AndroidElement popularbill_continue;
	
	@AndroidFindBy(className="android.widget.ImageButton")
	public AndroidElement backclick;
	
	}
