package com.safaricom.pages.bills;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class paybillHomePage {
	public AndroidDriver<MobileElement> driver;
  
  public paybillHomePage() {
	  
  }
  public paybillHomePage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_dp_toolbarTitle")
	public AndroidElement lipanampesa_title;
	
	//PayBill
		
	@AndroidFindBy(id="com.selfcare.safaricom:id/radio_pay_bill")
	public AndroidElement paybillClick;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/edt_pb_business_no")
	public AndroidElement paybill_edt_buss_no;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/edt_pb_account_no")
	public AndroidElement paybill_edt_account_number;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/edt_pb_amount")
	public AndroidElement paybill_edt_bill_amount;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/button_pb_next")
	public AndroidElement paybill_continue;
}
