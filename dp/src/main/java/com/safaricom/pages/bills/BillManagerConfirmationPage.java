package com.safaricom.pages.bills;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class BillManagerConfirmationPage {
 
	public AndroidDriver<MobileElement> driver;
	public BillManagerConfirmationPage()
	{
		
	}
	
	public BillManagerConfirmationPage(AndroidDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	

	@AndroidFindBy(id="com.selfcare.safaricom:id/labelTitle")
	public AndroidElement confirmation_title;
	
//	@AndroidFindBy(id="com.selfcare.safaricom:id/txt_pay_bill_to")
//	public AndroidElement pay_bill_to;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/txtpaybillnum")
	public AndroidElement txtpaybillnum;
		
	@AndroidFindBy(id="com.selfcare.safaricom:id/tvAccounttName")
	public AndroidElement tvAccounttName;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tvAccountNo")
	public AndroidElement tvAccountNo;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_amount")
	public AndroidElement tv_amount;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tvTransactionCost")
	public AndroidElement tvTransactionCost;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/buttonNegative")
	public AndroidElement buttonNegative;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/buttonPositive")
	public AndroidElement buttonPositive;
	
	
	
	
}
