package com.safaricom.report;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.safaricom.config.TestConfig;
import com.safaricom.test.AccountTest;
import com.safaricom.test.BillTest;
import com.safaricom.test.BongaServicesTest;
import com.safaricom.test.BuyAirtimeTest;
import com.safaricom.test.BuyGoodsAndServicesTest;
import com.safaricom.test.BuyPlanTest;
import com.safaricom.test.DPHomeTest;
import com.safaricom.test.DatansmsplansServicesTest;
import com.safaricom.test.DealHomeTest;
import com.safaricom.test.FlexServiceTest;
import com.safaricom.test.FulizampesaTest;
import com.safaricom.test.LipanaMpesaTest;
import com.safaricom.test.LoansandSavingsTest;
import com.safaricom.test.LoginTest;
import com.safaricom.test.MPESAMyAccountTest;
import com.safaricom.test.MPESASendMoneyTest;
import com.safaricom.test.MenuTest;
import com.safaricom.test.MpesaGlobalTest;
import com.safaricom.test.MpesaHomeTest;
import com.safaricom.test.MyAccountTest;
import com.safaricom.test.MyBillTest;
import com.safaricom.test.MySMSServicesTest;
import com.safaricom.test.OkoaServicesTest;
import com.safaricom.test.RoamingServicesTest;
import com.safaricom.test.SambazaServicesTest;
import com.safaricom.test.SendMoneyTest;
import com.safaricom.test.ServiceHomeTest;
import com.safaricom.test.SkizaServicesTest;
import com.safaricom.test.WithdrawCashTest;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class SafaricomReport_dp {
	ExtentReports extentReport;
	ExtentTest extentTest;
	LoginTest logintestestObject = new LoginTest();
	DPHomeTest dphometestObject=new DPHomeTest();
	AccountTest accounttestObject=new AccountTest();
	MpesaHomeTest mpesaHomeTestObject=new MpesaHomeTest();
	MPESASendMoneyTest mpesasendMoneyTestObject=new MPESASendMoneyTest();
	WithdrawCashTest withdawCashTestObject=new WithdrawCashTest();
	BuyAirtimeTest buyAirtimeTestObject=new BuyAirtimeTest();
	LipanaMpesaTest lipanaMpesaTestObject=new LipanaMpesaTest();
	LoansandSavingsTest loansandSavingsTestObject=new LoansandSavingsTest();
	MPESAMyAccountTest mpesamyAccountTestObject=new MPESAMyAccountTest();
	FulizampesaTest fulizampesaTestObject=new FulizampesaTest();
	MpesaGlobalTest mpesaGlobalTestObject=new MpesaGlobalTest();
	BuyPlanTest buyPlanTestObject=new BuyPlanTest();
	DealHomeTest dealHomeTestObject=new DealHomeTest();
	AccountTest accountTestObject=new AccountTest();
	BillTest billTestObject=new BillTest();
	SendMoneyTest sendMoneyTestObject=new SendMoneyTest();
	BuyGoodsAndServicesTest buyGoodsAndServicesTestObject=new BuyGoodsAndServicesTest();
MyAccountTest myAccountTestObject=new MyAccountTest();
ServiceHomeTest serviceHomeTestObject=new ServiceHomeTest();
BongaServicesTest bongaServicesTestOject=new BongaServicesTest();
FlexServiceTest flexServiceTestObject=new FlexServiceTest();
DatansmsplansServicesTest datansmsplansServicesTestObject=new DatansmsplansServicesTest();

	MySMSServicesTest mySMSServicesTestObject=new MySMSServicesTest();
	SambazaServicesTest sambazaServicesTestObject=new SambazaServicesTest();
	SkizaServicesTest skizaServicesTestObject=new SkizaServicesTest();
	RoamingServicesTest roamingServicesTestObject=new RoamingServicesTest();
	OkoaServicesTest okoaServicesTestObject=new OkoaServicesTest();
	MyBillTest myBillTestObject=new MyBillTest();
	MenuTest menuTestObject=new MenuTest();
	
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AppiumDriver<MobileElement> driver = TestConfig.getInstance().getDriver();

	@BeforeSuite
	public void beforeSuite() {
		// In before suite we are creating HTML report template, adding basic
		// information to it and load the extent-config.xml file

		extentReport = new ExtentReports(System.getProperty("user.dir") + "/Safaricom_Automation_dp_Report.html", true);
		extentReport.addSystemInfo("Host Name", "Safaricom").addSystemInfo("Environment", "Automation Testing")
				.addSystemInfo("User Name", "Merin Anna Mathew");
		extentReport.loadConfig(new File(System.getProperty("user.dir") + "/extent-config.xml"));
	}

	@BeforeClass
	public void beforeClass() {
		// WebDriverWait wait = TestConfig.getInstance().getWait();
		// AppiumDriver<MobileElement> driver = TestConfig.getInstance().getDriver();
	}

	@BeforeMethod
	public void beforeMethod(Method method) {
		// In before method we are collecting the current running test case name
		String className = this.getClass().getSimpleName();
		extentTest = extentReport.startTest(className + "-" + method.getName());
	}

	
	@Test(priority=1)
	public void LOGIN_TC_001() throws InterruptedException, IOException {

		logintestestObject.LOGIN_TC_001();
		Assert.assertTrue(true);
	}
	
	
	
//	@Test(priority=2)
//	public void DP_TC_001() throws InterruptedException, IOException {
//
//		dphometestObject.DP_TC_001();
//		Assert.assertTrue(true);
//	}
//	
	
//	@Test(priority=3)
//	public void MPESA_TC_001() throws InterruptedException, IOException {
//
//		mpesaHomeTestObject.MPESA_TC_001();
//		Assert.assertTrue(true);
//	}
//
//	@Test(priority=4)
//	public void MPESASENDMONEY_TC_001() throws InterruptedException, IOException {
//
//		mpesasendMoneyTestObject.MPESASENDMONEY_TC_001();
//		Assert.assertTrue(true);
//	}
//
//	@Test(priority=5)
//	public void MPESASENDMONEY_TC_002() throws InterruptedException, IOException {
//
//		mpesasendMoneyTestObject.MPESASENDMONEY_TC_002();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=6)
//	public void MPESASENDMONEY_TC_003() throws InterruptedException, IOException {
//
//		mpesasendMoneyTestObject.MPESASENDMONEY_TC_003();
//		Assert.assertTrue(true);
//	}

//	@Test(priority=7)
//	public void WITHDRAWCASH_TC_001() throws InterruptedException, IOException {
//
//		withdawCashTestObject.WITHDRAWCASH_TC_001();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=8)
//	public void WITHDRAWCASH_TC_002() throws InterruptedException, IOException {
//
//		withdawCashTestObject.WITHDRAWCASH_TC_002();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=9)
//	public void WITHDRAWCASH_TC_003() throws InterruptedException, IOException {
//
//		withdawCashTestObject.WITHDRAWCASH_TC_003();
//		Assert.assertTrue(true);
//	}
	
//	@Test(priority=10)
//	public void BUYAIRTIME_TC_001() throws InterruptedException, IOException {
//
//		buyAirtimeTestObject.BUYAIRTIME_TC_001();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=11)
//	public void BUYAIRTIME_TC_002() throws InterruptedException, IOException {
//
//		buyAirtimeTestObject.BUYAIRTIME_TC_002();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=12)
//	public void BUYAIRTIME_TC_003() throws InterruptedException, IOException {
//
//		buyAirtimeTestObject.BUYAIRTIME_TC_003();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=13)
//	public void LIPANAMPESA_TC_001() throws InterruptedException, IOException {
//		
//		lipanaMpesaTestObject.LIPANAMPESA_TC_001();
//		Assert.assertTrue(true);
//		
//	}
//
//	@Test(priority=14)
//	public void LIPANAMPESA_TC_002() throws InterruptedException, IOException {
//
//		lipanaMpesaTestObject.LIPANAMPESA_TC_002();
//		Assert.assertTrue(true);
//	}
//
//	@Test(priority=15)
//	public void LIPANAMPESA_TC_003() throws InterruptedException, IOException {
//
//		lipanaMpesaTestObject.LIPANAMPESA_TC_003();
//		Assert.assertTrue(true);
//	}
//
//	@Test(priority=16)
//	public void LOANSANDSAVINGS_TC_001() throws InterruptedException, IOException {
//
//		loansandSavingsTestObject.LOANSANDSAVINGS_TC_001();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=17)
//	public void LOANSANDSAVINGS_TC_002() throws InterruptedException, IOException {
//
//		loansandSavingsTestObject.LOANSANDSAVINGS_TC_002();
//		Assert.assertTrue(true);
//	}
//
//	@Test(priority=18)
//	public void LOANSANDSAVINGS_TC_003() throws InterruptedException, IOException {
//
//		loansandSavingsTestObject.LOANSANDSAVINGS_TC_003();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=19)
//	public void LOANSANDSAVINGS_TC_004() throws InterruptedException, IOException {
//
//		loansandSavingsTestObject.LOANSANDSAVINGS_TC_004();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=20)
//	public void LOANSANDSAVINGS_TC_005() throws InterruptedException, IOException {
//
//		loansandSavingsTestObject.LOANSANDSAVINGS_TC_005();
//		Assert.assertTrue(true);
//	}
//
//	@Test(priority=21)
//	public void LOANSANDSAVINGS_TC_006() throws InterruptedException, IOException {
//
//		loansandSavingsTestObject.LOANSANDSAVINGS_TC_006();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=22)
//	public void LOANSANDSAVINGS_TC_007() throws InterruptedException, IOException {
//
//		loansandSavingsTestObject.LOANSANDSAVINGS_TC_007();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=23)
//	public void LOANSANDSAVINGS_TC_008() throws InterruptedException, IOException {
//
//		loansandSavingsTestObject.LOANSANDSAVINGS_TC_008();
//		Assert.assertTrue(true);
//	}
//
//	@Test(priority=24)
//	public void LOANSANDSAVINGS_TC_009() throws InterruptedException, IOException {
//
//		loansandSavingsTestObject.LOANSANDSAVINGS_TC_009();
//		Assert.assertTrue(true);
//	}
//
//	@Test(priority=25)
//	public void LOANSANDSAVINGS_TC_010() throws InterruptedException, IOException {
//
//		loansandSavingsTestObject.LOANSANDSAVINGS_TC_010();
//		Assert.assertTrue(true);
//	}
	
//	@Test(priority=26)
//	public void LOANSANDSAVINGS_TC_011() throws InterruptedException, IOException {
//
//		loansandSavingsTestObject.LOANSANDSAVINGS_TC_011();
//		Assert.assertTrue(true);
//	}

//	@Test(priority=27)
//	public void LOANSANDSAVINGS_TC_012() throws InterruptedException, IOException {
//
//		loansandSavingsTestObject.LOANSANDSAVINGS_TC_012();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=28)
//	public void LOANSANDSAVINGS_TC_013() throws InterruptedException, IOException {
//
//		loansandSavingsTestObject.LOANSANDSAVINGS_TC_013();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=29)
//	public void LOANSANDSAVINGS_TC_014() throws InterruptedException, IOException {
//
//		loansandSavingsTestObject.LOANSANDSAVINGS_TC_014();
//		Assert.assertTrue(true);
//	}
	
//	@Test(priority=30)
//	public void MPESAMYACCOUNT_TC_001() throws InterruptedException, IOException {
//
//		mpesamyAccountTestObject.MPESAMYACCOUNT_TC_001();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=31)
//	public void MPESAMYACCOUNT_TC_002() throws InterruptedException, IOException {
//
//		mpesamyAccountTestObject.MPESAMYACCOUNT_TC_002();
//		Assert.assertTrue(true);
//	}
	
//	@Test(priority=32)
//	public void MPESAMYACCOUNT_TC_003() throws InterruptedException, IOException {
//
//		mpesamyAccountTestObject.MPESAMYACCOUNT_TC_003();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=33)
//	public void MPESAMYACCOUNT_TC_004() throws InterruptedException, IOException {
//
//		mpesamyAccountTestObject.MPESAMYACCOUNT_TC_004();
//		Assert.assertTrue(true);
//	}

//	@Test(priority=34)
//	public void FULIZAMPESA_TC_001() throws InterruptedException, IOException {
//
//		fulizampesaTestObject.FULIZAMPESA_TC_001();
//		Assert.assertTrue(true);
//	}

//	@Test(priority=35)
//	public void MPESAGLOBAL_TC_001() throws InterruptedException, IOException {
//
//		mpesaGlobalTestObject.MPESAGLOBAL_TC_001();
//		Assert.assertTrue(true);
//	}

//	@Test(priority=36)
//	public void BUYPLANS_TC_001() throws InterruptedException, IOException {
//
//		buyPlanTestObject.BUYPLANS_TC_001();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=37)
//	public void DEALS_TC_001() throws InterruptedException, IOException {
//
//		dealHomeTestObject.DEALS_TC_001();
//		Assert.assertTrue(true);
//	}

//	@Test(priority=38)
//	public void ACCOUNT_TC_001() throws InterruptedException, IOException {
//
//		accounttestObject.ACCOUNT_TC_001();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=39)
//	public void BILL_TC_001() throws InterruptedException, IOException {
//
//		billTestObject.BILL_TC_001();
//		Assert.assertTrue(true);
//	}
	
//	@Test(priority=40)
//	public void BILL_TC_002() throws InterruptedException, IOException {
//
//		billTestObject.BILL_TC_002();
//		Assert.assertTrue(true);
//	}
//	
//	
//	@Test(priority=41)
//	public void BILL_TC_003() throws InterruptedException, IOException {
//
//		billTestObject.BILL_TC_003();
//		Assert.assertTrue(true);
//	}
	
//	@Test(priority=42)
//	public void BILL_TC_004() throws InterruptedException, IOException {
//
//		billTestObject.BILL_TC_004();
//		Assert.assertTrue(true);
//	}
//	
//	
//	@Test(priority=43)
//	public void BILL_TC_005() throws InterruptedException, IOException {
//
//		billTestObject.BILL_TC_005();
//		Assert.assertTrue(true);
//	}
//
//	@Test(priority=44)
//	public void BILL_TC_006() throws InterruptedException, IOException {
//
//		billTestObject.BILL_TC_006();
//		Assert.assertTrue(true);
//	}
//
//	@Test(priority=45)
//	public void SENDMONEY_TC_001() throws InterruptedException, IOException {
//
//		sendMoneyTestObject.SENDMONEY_TC_001();
//		Assert.assertTrue(true);
//	}
	
//	@Test(priority=46)
//	public void SENDMONEY_TC_002() throws InterruptedException, IOException {
//
//		sendMoneyTestObject.SENDMONEY_TC_002();
//		Assert.assertTrue(true);
//	}

//	@Test(priority=47)
//	public void SENDMONEY_TC_003() throws InterruptedException, IOException {
//
//		sendMoneyTestObject.SENDMONEY_TC_003();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=48)
//	public void BUYGOODSNSERVICES_TC_001() throws InterruptedException, IOException {
//
//		buyGoodsAndServicesTestObject.BUYGOODSNSERVICES_TC_001();
//		Assert.assertTrue(true);
//	}
	
//	@Test(priority=49)
//	public void MYDATAUSAGE_TC_001() throws InterruptedException, IOException {
//
//	.MYDATAUSAGE_TC_001();
//		Assert.assertTrue(true);
//	}
//	

//	@Test(priority=50)
//	public void MENU_MYACCOUNT_TC_001() throws InterruptedException, IOException {
//
//		myAccountTestObject.MENU_MYACCOUNT_TC_001();
//		Assert.assertTrue(true);
//	}
	
//	@Test(priority=51)
//	public void MENU_MYACCOUNT_TC_002() throws InterruptedException, IOException {
//
//		myAccountTestObject.MENU_MYACCOUNT_TC_002();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=52)
//	public void MENU_MYACCOUNT_TC_003() throws InterruptedException, IOException {
//
//		myAccountTestObject.MENU_MYACCOUNT_TC_003();
//		Assert.assertTrue(true);
//	}
	
//	@Test(priority=53)
//	public void MENU_MYACCOUNT_TC_004() throws InterruptedException, IOException {
//
//		myAccountTestObject.MENU_MYACCOUNT_TC_004();
//		Assert.assertTrue(true);
//	}
//
//	@Test(priority=54)
//	public void MENU_MYACCOUNT_TC_005() throws InterruptedException, IOException {
//
//		myAccountTestObject.MENU_MYACCOUNT_TC_005();
//		Assert.assertTrue(true);
//	}

//	@Test(priority=55)
//	public void MENU_MYACCOUNT_TC_006() throws InterruptedException, IOException {
//
//		myAccountTestObject.MENU_MYACCOUNT_TC_006();
//		Assert.assertTrue(true);
//	}
//
//	@Test(priority=56)
//	public void MENU_MYACCOUNT_TC_007() throws InterruptedException, IOException {
//
//		myAccountTestObject.MENU_MYACCOUNT_TC_007();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=57)
//	public void MENU_MYACCOUNT_TC_008() throws InterruptedException, IOException {
//
//		myAccountTestObject.MENU_MYACCOUNT_TC_008();
//		Assert.assertTrue(true);
//	}

//	@Test(priority=58)
//	public void MENU_MYACCOUNT_TC_009() throws InterruptedException, IOException {
//
//		myAccountTestObject.MENU_MYACCOUNT_TC_009();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=59)
//	public void MENU_MYACCOUNT_TC_010() throws InterruptedException, IOException {
//
//		myAccountTestObject.MENU_MYACCOUNT_TC_010();
//		Assert.assertTrue(true);
//	}
	
//	@Test(priority=60)
//	public void MENU_MYACCOUNT_TC_011() throws InterruptedException, IOException {
//
//		myAccountTestObject.MENU_MYACCOUNT_TC_011();
//		Assert.assertTrue(true);
//	}

//	@Test(priority=61)
//	public void MENU_MYACCOUNT_TC_012() throws InterruptedException, IOException {
//
//		myAccountTestObject.MENU_MYACCOUNT_TC_012();
//		Assert.assertTrue(true);
//	}
	
//	@Test(priority=62)
//	public void MENU_MYACCOUNT_TC_013() throws InterruptedException, IOException {
//
//		myAccountTestObject.MENU_MYACCOUNT_TC_013();
//		Assert.assertTrue(true);
//	}
	
	
//	@Test(priority=63)
//	public void SERVICES_TC_001() throws InterruptedException, IOException {
//
//		serviceHomeTestObject.SERVICES_TC_001();
//		Assert.assertTrue(true);
//	}
//
//	@Test(priority=64)
//	public void MENU_BONGASERVICES_TC_001() throws InterruptedException, IOException {
//
//		bongaServicesTestOject.MENU_BONGASERVICES_TC_001();
//		Assert.assertTrue(true);
//	}

//	@Test(priority=65)
//	public void MENU_BONGASERVICES_TC_002() throws InterruptedException, IOException {
//
//		bongaServicesTestOject.MENU_BONGASERVICES_TC_002();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=66)
//	public void MENU_BONGASERVICES_TC_003() throws InterruptedException, IOException {
//
//		bongaServicesTestOject.MENU_BONGASERVICES_TC_003();
//		Assert.assertTrue(true);
//	}

//	@Test(priority=67)
//	public void MENU_BONGASERVICES_TC_004() throws InterruptedException, IOException {
//
//		bongaServicesTestOject.MENU_BONGASERVICES_TC_004();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=68)
//	public void MENU_BONGASERVICES_TC_005() throws InterruptedException, IOException {
//
//		bongaServicesTestOject.MENU_BONGASERVICES_TC_005();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=69)
//	public void MENU_FLEXSERVICES_TC_001() throws InterruptedException, IOException {
//
//		flexServiceTestObject.MENU_FLEXSERVICES_TC_001();
//		Assert.assertTrue(true);
//	}
	
//	@Test(priority=70)
//	public void MENU_FLEXSERVICES_TC_002() throws InterruptedException, IOException {
//
//		flexServiceTestObject.MENU_FLEXSERVICES_TC_002();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=71)
//	public void MENU_FLEXSERVICES_TC_003() throws InterruptedException, IOException {
//
//		flexServiceTestObject.MENU_FLEXSERVICES_TC_003();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=72)
//	public void MENU_FLEXSERVICES_TC_004() throws InterruptedException, IOException {
//
//		flexServiceTestObject.MENU_FLEXSERVICES_TC_004();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=73)
//	public void MENU_FLEXSERVICES_TC_005() throws InterruptedException, IOException {
//
//		flexServiceTestObject.MENU_FLEXSERVICES_TC_005();
//		Assert.assertTrue(true);
//	}
//
//	@Test(priority=74)
//	public void MENU_FLEXSERVICES_TC_006() throws InterruptedException, IOException {
//
//		flexServiceTestObject.MENU_FLEXSERVICES_TC_006();
//		Assert.assertTrue(true);
//	}
//
//	@Test(priority=75)
//	public void MENU_FLEXSERVICES_TC_007() throws InterruptedException, IOException {
//
//		flexServiceTestObject.MENU_FLEXSERVICES_TC_007();
//		Assert.assertTrue(true);
//	}

//	@Test(priority=76)
//	public void MENU_DATASMSPLANSSERVICES_TC_001() throws InterruptedException, IOException {
//
//		datansmsplansServicesTestObject.MENU_DATASMSPLANSSERVICES_TC_001();
//		Assert.assertTrue(true);
//	}
	
//	@Test(priority=77)
//	public void MENU_DATASMSPLANSSERVICES_TC_002() throws InterruptedException, IOException {
//
//		datansmsplansServicesTestObject.MENU_DATASMSPLANSSERVICES_TC_002();
//		Assert.assertTrue(true);
//	}
//
//	@Test(priority=78)
//	public void MENU_DATASMSPLANSSERVICES_TC_003() throws InterruptedException, IOException {
//
//		datansmsplansServicesTestObject.MENU_DATASMSPLANSSERVICES_TC_003();
//		Assert.assertTrue(true);
//	}
	
//	@Test(priority=79)
//	public void MENU_MYSMSSERVICES_TC_001() throws InterruptedException, IOException {
//
//		mySMSServicesTestObject.MENU_MYSMSSERVICES_TC_001();
//		Assert.assertTrue(true);
//	}
	
//	@Test(priority=80)
//	public void MENU_MYSMSSERVICES_TC_002() throws InterruptedException, IOException {
//
//		mySMSServicesTestObject.MENU_MYSMSSERVICES_TC_002();
//		Assert.assertTrue(true);
//	}
	
	
//	@Test(priority=81)
//	public void MENU_SAMBAZASERVICES_TC_001() throws InterruptedException, IOException {
//
//		sambazaServicesTestObject.MENU_SAMBAZASERVICES_TC_001();
//		Assert.assertTrue(true);
//	}
	
//	@Test(priority=82)
//	public void MENU_SAMBAZASERVICES_TC_002() throws InterruptedException, IOException {
//
//		sambazaServicesTestObject.MENU_SAMBAZASERVICES_TC_002();
//		Assert.assertTrue(true);
//	}
//	
//	
//	@Test(priority=83)
//	public void MENU_SAMBAZASERVICES_TC_003() throws InterruptedException, IOException {
//
//		sambazaServicesTestObject.MENU_SAMBAZASERVICES_TC_003();
//		Assert.assertTrue(true);
//	}
//
//	@Test(priority=84)
//	public void MENU_SKIZASERVICES_TC_001() throws InterruptedException, IOException {
//
//		skizaServicesTestObject.MENU_SKIZASERVICES_TC_001();
//		Assert.assertTrue(true);
//	}
//
//	@Test(priority=85)
//	public void MENU_ROAMINGSERVICES_TC_001() throws InterruptedException, IOException {
//
//		roamingServicesTestObject.MENU_ROAMINGSERVICES_TC_001();
//		Assert.assertTrue(true);
//	}
	
//	@Test(priority=86)
//	public void MENU_ROAMINGSERVICES_TC_002() throws InterruptedException, IOException {
//
//		roamingServicesTestObject.MENU_ROAMINGSERVICES_TC_002();
//		Assert.assertTrue(true);
//	}
//
//	@Test(priority=87)
//	public void MENU_ROAMINGSERVICES_TC_003() throws InterruptedException, IOException {
//
//		roamingServicesTestObject.MENU_ROAMINGSERVICES_TC_003();
//		Assert.assertTrue(true);
//	}
	
//	@Test(priority=88)
//	public void MENU_OKOASERVICES_TC_001() throws InterruptedException, IOException {
//
//		okoaServicesTestObject.MENU_OKOASERVICES_TC_001();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=89)
//	public void MENU_MYBILLS_TC_001() throws InterruptedException, IOException {
//
//		myBillTestObject.MENU_MYBILLS_TC_001();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=90)
//	public void MENU_MYBILLS_TC_002() throws InterruptedException, IOException {
//
//		myBillTestObject.MENU_MYBILLS_TC_002();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=91)
//	public void MENU_MYBILLS_TC_003() throws InterruptedException, IOException {
//
//		myBillTestObject.MENU_MYBILLS_TC_003();
//		Assert.assertTrue(true);
//	}
	
//	@Test(priority=92)
//	public void MENU_MYBILLS_TC_004() throws InterruptedException, IOException {
//
//		myBillTestObject.MENU_MYBILLS_TC_004();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=93)
//	public void MENU_MYBILLS_TC_005() throws InterruptedException, IOException {
//
//		myBillTestObject.MENU_MYBILLS_TC_005();
//		Assert.assertTrue(true);
//	}

//	@Test(priority=94)
//	public void MENU_MYBILLS_TC_006() throws InterruptedException, IOException {
//
//		myBillTestObject.MENU_MYBILLS_TC_006();
//		Assert.assertTrue(true);
//	}

//	@Test(priority=95)
//	public void MENU_TC_001() throws InterruptedException, IOException {
//
//		menuTestObject.MENU_TC_001();
//		Assert.assertTrue(true);
//	}
//
//	@Test(priority=96)
//	public void MENU_TC_002() throws InterruptedException, IOException {
//
//		menuTestObject.MENU_TC_002();
//		Assert.assertTrue(true);
//	}
	
//	@Test(priority=97)
//	public void MENU_TC_003() throws InterruptedException, IOException {
//
//		menuTestObject.MENU_TC_003();
//		Assert.assertTrue(true);
//	}
//
//	@Test(priority=98)
//	public void MENU_TC_004() throws InterruptedException, IOException {
//
//		menuTestObject.MENU_TC_004();
//		Assert.assertTrue(true);
//	}
	
//	@Test(priority=99)
//	public void MENU_TC_005() throws InterruptedException, IOException {
//
//		menuTestObject.MENU_TC_005();
//		Assert.assertTrue(true);
//	}
	
	

//	@Test(priority=100)
//	public void MENU_TC_006() throws InterruptedException, IOException {
//
//		menuTestObject.MENU_TC_006();
//		Assert.assertTrue(true);
//	}

	
//	@Test(priority=101)
//	public void MENU_TC_007() throws InterruptedException, IOException {
//
//		menuTestObject.MENU_TC_007();
//		Assert.assertTrue(true);
//	}
	
//	@Test(priority=102)
//	public void MENU_TC_008() throws InterruptedException, IOException {
//
//		menuTestObject.MENU_TC_008();
//		Assert.assertTrue(true);
//	}
	
	
//	@Test(priority=103)
//	public void MENU_TC_009() throws InterruptedException, IOException {
//
//		menuTestObject.MENU_TC_009();
//		Assert.assertTrue(true);
//	}

//	@Test(priority=104)
//	public void MENU_TC_010() throws InterruptedException, IOException {
//
//		menuTestObject.MENU_TC_010();
//		Assert.assertTrue(true);
//	}

//	@Test(priority=105)
//	public void MENU_TC_011() throws InterruptedException, IOException {
//
//		menuTestObject.MENU_TC_011();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=106)
//	public void MENU_TC_012() throws InterruptedException, IOException {
//
//		menuTestObject.MENU_TC_012();
//		Assert.assertTrue(true);
//	}

//	@Test(priority=107)
//	public void MENU_TC_013() throws InterruptedException, IOException {
//
//		menuTestObject.MENU_TC_013();
//		Assert.assertTrue(true);
//	}
	
//	@Test(priority=108)
//	public void MENU_TC_014() throws InterruptedException, IOException {
//
//		menuTestObject.MENU_TC_014();
//		Assert.assertTrue(true);
//	}
	
//	@Test(priority=109)
//	public void MENU_TC_015() throws InterruptedException, IOException {
//
//		menuTestObject.MENU_TC_015();
//		Assert.assertTrue(true);
//	}
//	
//	@Test(priority=110)
//	public void MENU_TC_016() throws InterruptedException, IOException {
//
//		menuTestObject.MENU_TC_016();
//		Assert.assertTrue(true);
//	}
	
	
	

	@AfterMethod
	public void getResult(ITestResult result, Method method) throws Exception {
		// In after method we are collecting the test execution status and based on that
		// the information writing to HTML report
		if (result.getStatus() == ITestResult.FAILURE) {
			extentTest.log(LogStatus.FAIL, "Test Case Failed is : " + result.getName());
			extentTest.log(LogStatus.FAIL, "Error Details :- "  +  result.getThrowable().getMessage() );
//			driver.launchApp();

		}
		if (result.getStatus() == ITestResult.SKIP) {
			extentTest.log(LogStatus.SKIP, "Test Case Skipped is : " + result.getName());
		}
		if (result.getStatus() == ITestResult.SUCCESS) {
			extentTest.log(LogStatus.PASS, "Test Case Passed is : " + result.getName());
//			driver.launchApp();
		}
	}

	@AfterSuite
	public void endReport() {
		// In after suite stopping the object of ExtentReports and ExtentTest
		extentReport.endTest(extentTest);
		extentReport.flush();
		driver.quit();
	}

	/**
	 * 
	 * To Capture the Screenshot and return the file path to extent report fail
	 * cases
	 *
	 * @param driver
	 * @param screenShotName
	 * @return
	 * @throws IOException
	 */
	public static String capture(AppiumDriver<MobileElement> driver, String screenShotName) throws IOException {
		String dest = null;
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy h-m-s");
			Date date = new Date();
			TakesScreenshot ts = driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
			dest = System.getProperty("user.dir") + "/ErrorScreenshots/" + screenShotName + dateFormat.format(date)
					+ ".png";
			File destination = new File(dest);
			FileUtils.copyFile(source, destination);
		} catch (Exception e) {
			e.getMessage();
			System.out.println(e.getMessage());
		}
		return dest;
	}
}
