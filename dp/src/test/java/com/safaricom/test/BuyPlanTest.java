package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.dphome.buyPlanHomePage;
import com.safaricom.pages.dphome.dpHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class BuyPlanTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public buyPlanHomePage buyPlanHomeObject=new buyPlanHomePage(driver);
	public dpHomePage dpHomeObject=new dpHomePage(driver);

	/*
	 * Verify whether the icons such as
	 * "CHECKBALANCE,M-PESA,TUNUKIWA,MY DATA USAGE,DATA AND SMS PLANS,PLATINUM PLANS,SERVICES,MY ACCOUNT AND CONTACT US "
	 * and the "BANNER" is displayed in SFC Home Page and Title should be "MENU"
	 */
	@Test
	public void BUYPLANS_TC_001() throws InterruptedException, IOException {

		wait.until(ExpectedConditions.elementToBeClickable(dpHomeObject.buy_plan)).click();
		Thread.sleep(10000);
 		Assert.assertEquals(true, buyPlanHomeObject.view_all_plans.isDisplayed());
 		wait.until(ExpectedConditions.elementToBeClickable(buyPlanHomeObject.view_all_plans)).click();
 		
	}

}
