package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.dphome.dpHomePage;
import com.safaricom.pages.menu.Menu_services;
import com.safaricom.pages.menu.Menudrawer;
import com.safaricom.pages.menu.sfcHomePage;
import com.safaricom.pages.services.DSPlanActivateConfirmationPage;
import com.safaricom.pages.services.DSPlanActivateFinalConfirmationPage;
import com.safaricom.pages.services.DataandsmsplanservicesPage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class DatansmsplansServicesTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();

	public sfcHomePage sfcHomePageObject = new sfcHomePage(driver);
	public Menudrawer menudrawerObject=new Menudrawer(driver);
	public Menu_services menuservicesObject=new Menu_services(driver);
	public dpHomePage dphomepageObject=new dpHomePage(driver);
	public DataandsmsplanservicesPage dataandsmsplanservicesPageObject=new DataandsmsplanservicesPage(driver);
	public DSPlanActivateConfirmationPage dsplanActivateComfirmationPageObject=new  DSPlanActivateConfirmationPage(driver);
	public DSPlanActivateFinalConfirmationPage dsplanActivateFinalComfirmationPageObject = new DSPlanActivateFinalConfirmationPage(driver);
	

	/*Verfiy whether 'ACTIVATE' option is present and the title is "DATA & SMS PLANS"*/
	
	@Test
	public void MENU_DATASMSPLANSSERVICES_TC_001() throws InterruptedException, IOException {

		Thread.sleep(10000);
		wait.until(ExpectedConditions.elementToBeClickable(dphomepageObject.menu_expand)).click();
		wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.services_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(menuservicesObject.datansmsplans_click)).click();
		String expected_maintitle = "DATA & SMS PLANS";
		String actual_maintitle = dataandsmsplanservicesPageObject.datasmsplan_title.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		Assert.assertEquals(true, dataandsmsplanservicesPageObject.activateClick.isDisplayed(), "Activate is not displayed");
		
	}

	/*Verify whether is it possible to Activate Data Plans using "ACTIVATE"*/
	
	@Test
	public void MENU_DATASMSPLANSSERVICES_TC_002() throws InterruptedException, IOException {

		Thread.sleep(10000);
		wait.until(ExpectedConditions.elementToBeClickable(dphomepageObject.menu_expand)).click();
		wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.services_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(menuservicesObject.datansmsplans_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(dataandsmsplanservicesPageObject.activateClick)).click();
		Assert.assertEquals(true, dsplanActivateComfirmationPageObject.confirmation_message.isDisplayed(),"Confirmation Mesage is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateComfirmationPageObject.tv_ok)).click();
		Assert.assertEquals(true, dsplanActivateFinalComfirmationPageObject.finalconfirmation_message.isDisplayed(),"Final Confirmation Mesage is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateFinalComfirmationPageObject.tv_ok)).click();
		
		
	}
	
	@Test
	public void MENU_DATASMSPLANSSERVICES_TC_003() throws InterruptedException, IOException {

		Thread.sleep(10000);
		wait.until(ExpectedConditions.elementToBeClickable(dphomepageObject.menu_expand)).click();
		wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.services_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(menuservicesObject.datansmsplans_click)).click();
	driver.navigate().back();
	driver.navigate().back();
		
	}
	
}
