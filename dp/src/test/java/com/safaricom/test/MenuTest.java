package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.backtoplatinum.Menu_backtoplatinum;
import com.safaricom.pages.backtoplatinum.Menu_backtoplatinumfinalConfirmation;
import com.safaricom.pages.contactus.ContactUSHomePage;
import com.safaricom.pages.contactus.ContactUS_NewRequest;
import com.safaricom.pages.contactus.ContactUS_NewRequestFinalConfirmation;
import com.safaricom.pages.dphome.dpHomePage;
import com.safaricom.pages.menu.DateSelector;
import com.safaricom.pages.menu.MenuDataUsagePage;
import com.safaricom.pages.menu.MenuMyBillHomePage;
import com.safaricom.pages.menu.MenuMyaccountHomePage;
import com.safaricom.pages.menu.Menu_LoginPage;
import com.safaricom.pages.menu.Menu_LogoutConfirmation;
import com.safaricom.pages.menu.Menu_SetServicePinConf;
import com.safaricom.pages.menu.Menu_SetServicePinFinalConf;
import com.safaricom.pages.menu.Menu_SettingsChangeServicePin;
import com.safaricom.pages.menu.Menu_SettingsChangeServicePinConf;
import com.safaricom.pages.menu.Menu_SettingsSetServicePin;
import com.safaricom.pages.menu.Menu_backtoSafaricomApp;
import com.safaricom.pages.menu.Menu_backtoSafaricomAppConfirmation;
import com.safaricom.pages.menu.Menu_feedback;
import com.safaricom.pages.menu.Menu_feedbackConfirmation;
import com.safaricom.pages.menu.Menu_knowledgebase;
import com.safaricom.pages.menu.Menu_services;
import com.safaricom.pages.menu.Menu_setting;
import com.safaricom.pages.menu.Menu_storelocator;
import com.safaricom.pages.menu.Menu_tellafriend;
import com.safaricom.pages.menu.Menudrawer;
import com.safaricom.pages.menu.sfcHomePage;
import com.safaricom.pages.myaccount.MyAccount_BalanceHomePage;
import com.safaricom.pages.myaccount.MyAccount_PukHomePage;
import com.safaricom.pages.myaccount.MyAccount_TopupHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class MenuTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public dpHomePage dpHomePageObject=new dpHomePage(driver);
	public Menudrawer menudrawerObject=new Menudrawer(driver);
	public ContactUSHomePage contactUshomePageObject=new ContactUSHomePage(driver);
	public ContactUS_NewRequest contactUS_NewRequestObject=new ContactUS_NewRequest(driver);
	public ContactUS_NewRequestFinalConfirmation  contactUS_NewRequestFinalConfirmationObject=new ContactUS_NewRequestFinalConfirmation(driver);
	public Menu_services menuservicesObject=new Menu_services(driver);	
	public MenuMyaccountHomePage myaccounthomePageObject=new MenuMyaccountHomePage(driver);
	public DateSelector dateSelectorObject=new DateSelector(driver);
	public MyAccount_BalanceHomePage myaccount_BalanceHomePageObject=new MyAccount_BalanceHomePage(driver);
	public MyAccount_PukHomePage myaccountpukhomepageObject=new MyAccount_PukHomePage(driver);
	public MyAccount_TopupHomePage myaccounttopuphomepageObject=new MyAccount_TopupHomePage(driver);
	public Menu_knowledgebase menuknowledgebaseObject=new Menu_knowledgebase(driver);
	public Menu_storelocator menustorelocatorObject=new Menu_storelocator(driver);
	public Menu_tellafriend menutellafriendObject=new Menu_tellafriend(driver);
	public Menu_feedback  menufeedbackObject=new Menu_feedback(driver);
	public Menu_feedbackConfirmation menufeedbackconfirmationObject=new Menu_feedbackConfirmation(driver);
	public sfcHomePage sfcHomePageObject=new sfcHomePage(driver);
	public MenuDataUsagePage menuDataUsagePageObject=new MenuDataUsagePage(driver);
	public Menu_setting menusettingObject = new Menu_setting(driver);
	public Menu_SettingsChangeServicePin menusettingchangeservicepinObject=new Menu_SettingsChangeServicePin(driver);
	public Menu_SettingsChangeServicePinConf menusettingchangeservicepinconfObject =new Menu_SettingsChangeServicePinConf(driver);
	public Menu_SettingsSetServicePin menusetservicepinObject=new Menu_SettingsSetServicePin(driver);
	public Menu_SetServicePinConf menusetservicepinConfObject=new Menu_SetServicePinConf(driver);
	public Menu_SetServicePinFinalConf menusetservicepinfinalConfObject=new Menu_SetServicePinFinalConf(driver);
	public MenuMyBillHomePage menumyBillHomePageObject=new MenuMyBillHomePage(driver);
	public Menu_backtoSafaricomApp menu_backtoSafaricom=new Menu_backtoSafaricomApp(driver);
	public Menu_backtoSafaricomAppConfirmation menu_backtoSafaricomfinalConfirmationObject=new Menu_backtoSafaricomAppConfirmation(driver);
	public Menu_backtoplatinum menu_backtoplatinum=new Menu_backtoplatinum(driver);
	public Menu_backtoplatinumfinalConfirmation menu_backtoplatinumfinalConfirmationObject=new Menu_backtoplatinumfinalConfirmation(driver);
	public Menu_LogoutConfirmation menuLogoutConfirmationObject=new Menu_LogoutConfirmation(driver);
	public Menu_LoginPage menuLoginPageObject=new Menu_LoginPage(driver);


	@Test
	public void MENU_TC_001() throws InterruptedException, IOException {

		wait.until(ExpectedConditions.elementToBeClickable(dpHomePageObject.menu_expand)).click();
		MobileElement myAccount = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"MY ACCOUNT\"))");
		Assert.assertEquals(true, myAccount.isDisplayed());
		MobileElement service = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"SERVICES\"))");
		Assert.assertEquals(true, service.isDisplayed());
		MobileElement myBill = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"MY BILL\"))");
		Assert.assertEquals(true, myBill.isDisplayed());
		MobileElement dataUsage = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"DATA USAGE\"))");
		Assert.assertEquals(true, dataUsage.isDisplayed());
		MobileElement knowledgeBase = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"KNOWLEDGE BASE\"))");
		Assert.assertEquals(true, knowledgeBase.isDisplayed());
		MobileElement storeLocator = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"STORE LOCATOR\"))");
		Assert.assertEquals(true, storeLocator.isDisplayed());
		MobileElement tellafriend = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"TELL A FRIEND\"))");
		Assert.assertEquals(true, tellafriend.isDisplayed());
		MobileElement feedbacknrating = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"FEEDBACK & RATING\"))");
		Assert.assertEquals(true, feedbacknrating.isDisplayed());
		MobileElement contactUs = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"CONTACT US\"))");
		Assert.assertEquals(true, contactUs.isDisplayed());
		MobileElement settings = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"SETTINGS\"))");
		Assert.assertEquals(true, settings.isDisplayed());
		MobileElement backtosafaricomApp = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"BACK TO SAFARICOM APP\"))");
		Assert.assertEquals(true, backtosafaricomApp.isDisplayed());
		MobileElement logOut = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"LOGOUT\"))");
		Assert.assertEquals(true, logOut.isDisplayed());
		
	}
	
	
		@Test
	public void MENU_TC_002() throws InterruptedException, IOException {

			wait.until(ExpectedConditions.elementToBeClickable(dpHomePageObject.menu_expand)).click();
			MobileElement myAccount = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
					+ "new UiSelector().text(\"MY ACCOUNT\"))");
			myAccount.click();
//			wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.myAccount_click)).click();
		
		Thread.sleep(1000);
		String expectedtitle = "MY ACCOUNT";
		String actualtitle = myaccounthomePageObject.txt_title.getText();
		Assert.assertEquals(actualtitle, expectedtitle);
		Assert.assertEquals(true, myaccounthomePageObject.balance_tab_click.isDisplayed(),"Balance tab is displayed");
		Assert.assertEquals(true, myaccounthomePageObject.puk_click.isDisplayed(),"PUK tab is displayed");
		Assert.assertEquals(true, myaccounthomePageObject.topup_tab_click.isDisplayed(),"Top Up tab is displayed");
		wait.until(ExpectedConditions.elementToBeClickable(myaccounthomePageObject.balance_tab_click)).click();
		Assert.assertEquals(true, myaccounthomePageObject.balance_tab_click.isSelected(),"Balance tab is selected");
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.data_balance_view.isDisplayed());
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.bonga_balance_view.isDisplayed());
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.airtime_balance_view.isDisplayed());
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.sms_balance_view.isDisplayed());
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.okoa_balance_view.isDisplayed());
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.voice_balance_view.isDisplayed());
		
		wait.until(ExpectedConditions.elementToBeClickable(myaccount_BalanceHomePageObject.data_balance_view)).click();
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.limited_data_amount.isDisplayed());
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.daily_data_amount.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(myaccount_BalanceHomePageObject.data_balance_view)).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(myaccount_BalanceHomePageObject.bonga_balance_view)).click();
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.bonga_balance.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(myaccount_BalanceHomePageObject.bonga_balance_view)).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(myaccount_BalanceHomePageObject.airtime_balance_view)).click();
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.airtime_prepaid_balance.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(myaccount_BalanceHomePageObject.airtime_balance_view)).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(myaccount_BalanceHomePageObject.sms_balance_view)).click();
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.daily_sms_balance.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(myaccount_BalanceHomePageObject.sms_balance_view)).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(myaccount_BalanceHomePageObject.okoa_balance_view)).click();
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.okoa_airtime_balance.isDisplayed());
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.okoa_data_balance.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(myaccount_BalanceHomePageObject.okoa_balance_view)).click();
				
		wait.until(ExpectedConditions.elementToBeClickable(myaccount_BalanceHomePageObject.voice_balance_view)).click();
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.voice_on_net_talktime.isDisplayed());
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.voice_off_net_talktime.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(myaccount_BalanceHomePageObject.voice_balance_view)).click();
		
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.redeemedmms_txt_amount.isDisplayed());
		Assert.assertEquals(true, myaccount_BalanceHomePageObject.redeemedmms_txt_date.isDisplayed());
		
		
		
		wait.until(ExpectedConditions.elementToBeClickable(myaccounthomePageObject.puk_click)).click();
		Assert.assertEquals(true, myaccounthomePageObject.puk_click.isSelected(),"PUK tab is selected");
		Assert.assertEquals(true, myaccountpukhomepageObject.labelRequestPUK.isDisplayed());
		Assert.assertEquals(true, myaccountpukhomepageObject.rb_my_num_puk.isDisplayed());
		Assert.assertEquals(true, myaccountpukhomepageObject.rb_other_num_puk.isDisplayed());
		Assert.assertEquals(true, myaccountpukhomepageObject.tv_topup_btn.isDisplayed());	
		
		
		
		wait.until(ExpectedConditions.elementToBeClickable(myaccounthomePageObject.topup_tab_click)).click();
		
		Assert.assertEquals(true, myaccounttopuphomepageObject.txt_topup_asat.isDisplayed());
		Assert.assertEquals(true, myaccounttopuphomepageObject.tv_airtime_balance.isDisplayed());
		Assert.assertEquals(true, myaccounttopuphomepageObject.btn_m_pesa_topup_click.isDisplayed());
		Assert.assertEquals(true, myaccounttopuphomepageObject.btn_redeem_bonga_click.isDisplayed());
		Assert.assertEquals(true, myaccounttopuphomepageObject.topup_my_number_click.isDisplayed());
		Assert.assertEquals(true, myaccounttopuphomepageObject.topup_other_number_click.isDisplayed());
		

	}

	@Test
	public void MENU_TC_003() throws InterruptedException, IOException {

		wait.until(ExpectedConditions.elementToBeClickable(dpHomePageObject.menu_expand)).click();
		MobileElement service = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"SERVICES\"))");
		service.click();
		//wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.services_click)).click();
		String expected_confirmation_title = "SERVICES";
		String actual_confirmation_title = menuservicesObject.services_title.getText();
		Assert.assertEquals(actual_confirmation_title, expected_confirmation_title);
		Assert.assertEquals(true, menuservicesObject.bongaservice_click.isDisplayed(),"Bonga Services icon is not displayed");
		Assert.assertEquals(true, menuservicesObject.flexServices_click.isDisplayed(),"Flex icon is not displayed");
		Assert.assertEquals(true, menuservicesObject.datansmsplans_click.isDisplayed(),"Data and SMS Plans icon is not displayed");
		Assert.assertEquals(true, menuservicesObject.mysmsservices_click.isDisplayed(),"My SMS Services icon is not displayed");
		Assert.assertEquals(true, menuservicesObject.sambazaservices_click.isDisplayed(),"Sambaza Services icon is not displayed");
		Assert.assertEquals(true, menuservicesObject.skizaservices_click.isDisplayed(),"Skiza Services icon is not displayed");
		Assert.assertEquals(true, menuservicesObject.roamingservices_click.isDisplayed(),"Roaming Services icon is not displayed");
		Assert.assertEquals(true, menuservicesObject.okoaservices_click.isDisplayed(),"Okoa Services icon is not displayed");

	}
	
	
	@Test
	public void MENU_TC_004() throws InterruptedException, IOException {

	
		wait.until(ExpectedConditions.elementToBeClickable(dpHomePageObject.menu_expand)).click();
		MobileElement myBill = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"MY BILL\"))");
		myBill.click();
		String expectedtitle = "MY BILLS";
		String actualtitle = menumyBillHomePageObject.myBill_title.getText();
		Assert.assertEquals(actualtitle, expectedtitle);
		
		Assert.assertEquals(true, menumyBillHomePageObject.myBill_label.isDisplayed());
		Assert.assertEquals(true, menumyBillHomePageObject.txt_bill_usage_balance.isDisplayed());
		Assert.assertEquals(true, menumyBillHomePageObject.txt_bill_prepayment.isDisplayed());
		Assert.assertEquals(true, menumyBillHomePageObject.txt_bill_credit_limit.isDisplayed());
		Assert.assertEquals(true, menumyBillHomePageObject.tv_requestforbill.isDisplayed());
		Assert.assertEquals(true, menumyBillHomePageObject.buttonPayTheBill.isDisplayed());
		Assert.assertEquals(true, menumyBillHomePageObject.accountBalance_label.isDisplayed());
		Assert.assertEquals(true, menumyBillHomePageObject.txt_usage_account_asat.isDisplayed());
		Assert.assertEquals(true, menumyBillHomePageObject.txt_voice_balance.isDisplayed());
		Assert.assertEquals(true, menumyBillHomePageObject.txt_data_balance.isDisplayed());
		Assert.assertEquals(true, menumyBillHomePageObject.txt_sms_balance.isDisplayed());
		Assert.assertEquals(true, menumyBillHomePageObject.btn_redeem_bonga.isDisplayed());
		Assert.assertEquals(true, menumyBillHomePageObject.btn_buy_data.isDisplayed());
		Assert.assertEquals(true, menumyBillHomePageObject.prepadiUsageStatement_title.isDisplayed());
		Assert.assertEquals(true, menumyBillHomePageObject.buttonFullStatement.isDisplayed());
		Assert.assertEquals(true, menumyBillHomePageObject.buttonMiniStatement.isDisplayed());

	}
	
	

	@Test
	public void MENU_TC_005() throws InterruptedException, IOException {

	
		Thread.sleep(5000);
		wait.until(ExpectedConditions.elementToBeClickable(dpHomePageObject.menu_expand)).click();
		MobileElement dataUsage = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"DATA USAGE\"))");
				
		dataUsage.click();
		
		Thread.sleep(5000);
		String expectedtitle = "DATA USAGE";
		String actualtitle = menuDataUsagePageObject.dataUsage_title.getText();
		Assert.assertEquals(actualtitle, expectedtitle);
		menuDataUsagePageObject.startDate.click();
		dateSelectorObject.ok_button.click();
		menuDataUsagePageObject.endDate.click();
		dateSelectorObject.ok_button.click();
		Assert.assertEquals(true,menuDataUsagePageObject.totalDataUsed.isDisplayed());
	
			}
	
	
	@Test
	public void MENU_TC_006() throws InterruptedException, IOException {

		wait.until(ExpectedConditions.elementToBeClickable(dpHomePageObject.menu_expand)).click();
		MobileElement knowledgeBase = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"KNOWLEDGE BASE\"))");
		knowledgeBase.click();

		Thread.sleep(1000);
		Assert.assertEquals(true,menuknowledgebaseObject.KnowledgeBase_logo.isDisplayed());
		
	}

	
	@Test
	public void MENU_TC_007() throws InterruptedException, IOException {

		Thread.sleep(1000);
		
		wait.until(ExpectedConditions.elementToBeClickable(dpHomePageObject.menu_expand)).click();
		MobileElement storeLocator = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"STORE LOCATOR\"))");
		storeLocator.click();
			//wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.storelocator_click)).click();
//			wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.L1.get(9))).click();
		
		Thread.sleep(1000);
		String expected_maintitle = "SHOP LOCATOR";
		String actual_maintitle = menustorelocatorObject.shoplocator_title.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		Assert.assertEquals(true, menustorelocatorObject.textViewGroupName.isDisplayed(), "Group name is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(menustorelocatorObject.textViewGroupName)).click();
		Assert.assertEquals(true, menustorelocatorObject.txt_name.isDisplayed(), "Name is not displayed");
		Assert.assertEquals(true, menustorelocatorObject.txt_address.isDisplayed(), "Address is not displayed");
		Assert.assertEquals(true, menustorelocatorObject.txt_email.isDisplayed(), "Email is not displayed");
		Assert.assertEquals(true, menustorelocatorObject.txt_weekday.isDisplayed(), "Weekday is not displayed");
		Assert.assertEquals(true, menustorelocatorObject.txt_weekend.isDisplayed(), "Weekend is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(menustorelocatorObject.txt_name)).click();
		driver.navigate().back();
		driver.navigate().back();
		
		
	}
	
	@Test
	public void MENU_TC_008() throws InterruptedException, IOException {

		Thread.sleep(1000);
	
		wait.until(ExpectedConditions.elementToBeClickable(dpHomePageObject.menu_expand)).click();
			MobileElement tellafriend = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
					+ "new UiSelector().text(\"TELL A FRIEND\"))");
			tellafriend.click();
//			wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.tellafriend)).click();
			//wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.L1.get(10))).click();
		
		String expected_maintitle = "Tell a friend";
		String actual_maintitle = menutellafriendObject.tellafriendtitle.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		Assert.assertEquals(true, menutellafriendObject.tellafriendicon.isDisplayed());
		driver.navigate().back();
	}

	@Test
	public void MENU_TC_009() throws InterruptedException, IOException {

		

		wait.until(ExpectedConditions.elementToBeClickable(dpHomePageObject.menu_expand)).click();
		MobileElement feedbacknrating = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"FEEDBACK & RATING\"))");
		feedbacknrating.click();
	//		wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.feedbacknrating_click)).click();
			//	wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.L1.get(11))).click();
		
		String expected_maintitle = "FEEDBACK & RATING";
		String actual_maintitle = menufeedbackObject.feedback_title.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		wait.until(ExpectedConditions.elementToBeClickable(menufeedbackObject.feedback_category_spinner)).click();
		wait.until(ExpectedConditions.elementToBeClickable(menufeedbackObject.feedback_Category_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(menufeedbackObject.feedback_email)).sendKeys("merin.ann@flytxt.com");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(menufeedbackObject.feedback_subject)).sendKeys("Trasaction");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(menufeedbackObject.feedback_msg)).sendKeys("Trasaction submit");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(menufeedbackObject.feedback_submit)).click();
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(menufeedbackconfirmationObject.ok_btn)).click();
		driver.navigate().back();
		
	}
	
	
	
	@Test
	public void MENU_TC_010() throws InterruptedException, IOException {

		Thread.sleep(1000);
		
	
		wait.until(ExpectedConditions.elementToBeClickable(dpHomePageObject.menu_expand)).click();
		MobileElement contactUs = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"CONTACT US\"))");
		contactUs.click();
		String expected_maintitle = "CONTACT US";
		String actual_maintitle = contactUshomePageObject.contactUs_title.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		Assert.assertEquals(true, contactUshomePageObject.img_twitter_click.isDisplayed(), "Twitter is not displayed");
		Assert.assertEquals(true, contactUshomePageObject.img_facebook_click.isDisplayed(),"Facebook is not displayed");
		Assert.assertEquals(true, contactUshomePageObject.img_message_click.isDisplayed(), "Message is not displayed");
		Assert.assertEquals(true, contactUshomePageObject.new_request_click.isDisplayed(), "New Request button is not displayed");
		Assert.assertEquals(true, contactUshomePageObject.txt_youtube_click.isDisplayed(), "Youtube is not displayed");
		Assert.assertEquals(true, contactUshomePageObject.txt_blog_click.isDisplayed(), "Blog is not displayed");
		Assert.assertEquals(true, contactUshomePageObject.txt_google_plus_click.isDisplayed(), "Google Plus is not displayed");
		
		wait.until(ExpectedConditions.elementToBeClickable(contactUshomePageObject.new_request_click)).click();
		
		String expected_subtitle = "REQUEST";
		String actual_subtitle = contactUS_NewRequestObject.txt_title.getText();
		Assert.assertEquals(actual_subtitle, expected_subtitle);
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestObject.request_area_spinner)).click();
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestObject.request_area)).click();
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestObject.request_sub_area_spinner)).click();
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestObject.request_sub_area)).click();
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestObject.requst_msg)).sendKeys("New Request");
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestObject.request_submit)).click();
		wait.until(ExpectedConditions.elementToBeClickable(contactUS_NewRequestFinalConfirmationObject.tv_ok)).click();
	}
	
	
	@Test
	public void MENU_TC_011() throws InterruptedException, IOException {

		wait.until(ExpectedConditions.elementToBeClickable(dpHomePageObject.menu_expand)).click();
		MobileElement settings = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"SETTINGS\"))");
		settings.click();
		Thread.sleep(1000);
		String expectedheader="SETTINGS";
		String actual_header=menusettingObject.settings_title.getText();
		Assert.assertEquals(actual_header, expectedheader);
		Assert.assertEquals(true,menusettingObject.changeservicepin_click.isDisplayed());
		Assert.assertEquals(true,menusettingObject.securewithpin_clicks.isDisplayed());
		
	}
	
	
	@Test
	public void MENU_TC_012() throws InterruptedException, IOException {

		
		
	}
	
	
	@Test
	public void MENU_TC_013() throws InterruptedException, IOException {

		wait.until(ExpectedConditions.elementToBeClickable(dpHomePageObject.menu_expand)).click();
		MobileElement settings = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"SETTINGS\"))");
		settings.click();
		wait.until(ExpectedConditions.elementToBeClickable(menusettingObject.changeservicepin_click)).click();
		Thread.sleep(1000);
		String expectedheader="Change Service PIN";
		String actual_header=menusettingchangeservicepinObject.changeservicepin_title.getText();
		Assert.assertEquals(actual_header, expectedheader);
		wait.until(ExpectedConditions.elementToBeClickable(menusettingchangeservicepinObject.et_old_pin)).sendKeys("1234");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(menusettingchangeservicepinObject.et_national_id)).sendKeys("1234567890");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(menusettingchangeservicepinObject.et_new_pin)).sendKeys("1221");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(menusettingchangeservicepinObject.et_confirm_pin)).sendKeys("1221");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(menusettingchangeservicepinObject.buttonSubmit)).click();
		wait.until(ExpectedConditions.elementToBeClickable(menusettingchangeservicepinconfObject.tv_ok)).click();
		
	}

	/*
	 * Verify whether it displays confirmation message to set PIN. It Should display
	 * the message for set PIN if PIN is not set.
	 */
	@Test
	public void MENU_TC_014() throws InterruptedException, IOException {

		wait.until(ExpectedConditions.elementToBeClickable(dpHomePageObject.menu_expand)).click();
		MobileElement settings = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"SETTINGS\"))");
		settings.click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(menusettingObject.securewithpin_clicks)).click();
		wait.until(ExpectedConditions.elementToBeClickable(menusetservicepinObject.ok_btn)).click();
		String expectedheader="Enter the Service PIN";
		String actual_header=menusetservicepinConfObject.setservicepin_header.getText();
		wait.until(ExpectedConditions.elementToBeClickable(menusetservicepinConfObject.et_pin)).sendKeys("1234");
		wait.until(ExpectedConditions.elementToBeClickable(menusetservicepinConfObject.btn_ok)).click();
		wait.until(ExpectedConditions.elementToBeClickable(menusetservicepinfinalConfObject.tv_ok)).click();
		
	}

	@Test
	public void MENU_TC_015() throws InterruptedException, IOException {

		Thread.sleep(1000);
		

		wait.until(ExpectedConditions.elementToBeClickable(dpHomePageObject.menu_expand)).click();
		MobileElement backtosafaricomApp = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"BACK TO SAFARICOM APP\"))");
		backtosafaricomApp.click();
					wait.until(ExpectedConditions.elementToBeClickable(menu_backtoSafaricom.ok_button)).click();
		wait.until(ExpectedConditions.elementToBeClickable(menu_backtoSafaricomfinalConfirmationObject.okbutton)).click();
		wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.menutab_click)).click();
		MobileElement elementToClick1 = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.selfcare.safaricom:id/ll_listView\")).scrollIntoView("
				+ "new UiSelector().text(\"Back to Platinum\"))");
		elementToClick1.click();
		wait.until(ExpectedConditions.elementToBeClickable(menu_backtoplatinum.ok_button)).click();
		wait.until(ExpectedConditions.elementToBeClickable(menu_backtoplatinumfinalConfirmationObject.okbutton)).click();
		
	}

	/*
	 * Verify whether it display the confirmation message, tap on yes should log out
	 * from the app & tap on no should close the pop up.
	 */
	
	@Test
	public void MENU_TC_016() throws InterruptedException, IOException {

		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(sfcHomePageObject.menutab_click)).click();
		MobileElement elementToClick1 = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.selfcare.safaricom:id/ll_listView\")).scrollIntoView("
				+ "new UiSelector().text(\"Back to Platinum\"))");
		elementToClick1.click();
		wait.until(ExpectedConditions.elementToBeClickable(menu_backtoplatinum.ok_button)).click();
		wait.until(ExpectedConditions.elementToBeClickable(menu_backtoplatinumfinalConfirmationObject.okbutton)).click();
		wait.until(ExpectedConditions.elementToBeClickable(dpHomePageObject.menu_expand)).click();
		MobileElement logOut = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
				+ "new UiSelector().text(\"LOGOUT\"))");
			logOut.click();
	wait.until(ExpectedConditions.elementToBeClickable(menuLogoutConfirmationObject.tv_ok)).click();
		String expectedtitle = "WELCOME TO";
		String actualtitle = menuLoginPageObject.login_welcome_message.getText();
		Assert.assertEquals(actualtitle, expectedtitle);
		Assert.assertEquals(true, menuLoginPageObject.tv_generate_pin.isDisplayed());
		System.out.println("Logout Success");
	}

}
