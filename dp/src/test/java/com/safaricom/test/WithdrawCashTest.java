package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.dphome.MpesaHomePage;
import com.safaricom.pages.dphome.dpHomePage;
import com.safaricom.pages.dphome.sfcHomePage;
import com.safaricom.pages.mpesa.WithdrawCashConfirmationPage;
import com.safaricom.pages.mpesa.WithdrawCashFinalConfirmationPage;
import com.safaricom.pages.mpesa.WithdrawCashPage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class WithdrawCashTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public sfcHomePage sfcHomePageObject = new sfcHomePage(driver);
	public dpHomePage dphomePageObject=new dpHomePage(driver);
	public MpesaHomePage mpesaHomePageObject = new MpesaHomePage(driver);
	public WithdrawCashPage withdrawCashPageObject = new WithdrawCashPage(driver);
	public WithdrawCashConfirmationPage withdrawCashConfirmationPageObject = new WithdrawCashConfirmationPage(driver);
	public WithdrawCashFinalConfirmationPage withdrawCashFinalConfirmationPageObject = new WithdrawCashFinalConfirmationPage(
			driver);

	/*
	 * Verfy whether the elements "FROM AGENT,FROM ATM" is present in Withdraw Cash
	 * Home Page and Title should be "WITHDRAW CASH"
	 */

	@Test
	public void WITHDRAWCASH_TC_001() throws InterruptedException, IOException {

		
		wait.until(ExpectedConditions.elementToBeClickable(dphomePageObject.mpesa_click)).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.withdraCashClick)).click();
		String expectedtitle = "WITHDRAW";
		String withdrawcash_title = withdrawCashPageObject.withdrawcash_title.getText();
		System.out.println(withdrawcash_title);
		Assert.assertEquals(withdrawcash_title, expectedtitle);
		wait.until(ExpectedConditions.elementToBeClickable(withdrawCashPageObject.withdrawFromAgent_click)).click();
		Assert.assertEquals(true, withdrawCashPageObject.edt_mobilenumber_WFAgent.isDisplayed(),"Enter mobile number field for WFAgent is not displayed");
		Assert.assertEquals(true, withdrawCashPageObject.et_amount_WFAgent.isDisplayed(),"Enter amount field for WFAgent is not displayed");
		Assert.assertEquals(true, withdrawCashPageObject.WFAgentContinueClick.isDisplayed(),"Continue button for WFAgent is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(withdrawCashPageObject.withdrawFromATM_click)).click();
		Assert.assertEquals(true, withdrawCashPageObject.edt_agentnumber_WFATM.isDisplayed(),"Enter Mobile number field for WFATM is not displayed ");
		Assert.assertEquals(true, withdrawCashPageObject.WFATMContinueClick.isDisplayed(),"Continue button for WFATM is not displayed");

	}

	/*
	 * Verify whether the "Withdraw From Agent" transaction is getting completed
	 * using valid Agent number and valid Amount
	 */

	@Test
	public void WITHDRAWCASH_TC_002() throws InterruptedException, IOException {
	
			wait.until(ExpectedConditions.elementToBeClickable(dphomePageObject.mpesa_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.withdraCashClick)).click();
		
		if (!(withdrawCashPageObject.edt_mobilenumber_WFAgent.isSelected())) {
			wait.until(ExpectedConditions.elementToBeClickable(withdrawCashPageObject.withdrawFromAgent_click)).click();
		}

		wait.until(ExpectedConditions.elementToBeClickable(withdrawCashPageObject.edt_mobilenumber_WFAgent))
				.sendKeys("790771777");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(withdrawCashPageObject.et_amount_WFAgent)).sendKeys("200");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(withdrawCashPageObject.WFAgentContinueClick)).click();
		Thread.sleep(10000);
		String expected_confirmation_title ="CONFIRM";
		String actual_confirmation_title = withdrawCashConfirmationPageObject.confirmation_title.getText();
		Assert.assertEquals(actual_confirmation_title, expected_confirmation_title);
		String mpesa_mobile_value = withdrawCashConfirmationPageObject.txt_dialog_account_number.getText();
		Assert.assertEquals(false, mpesa_mobile_value.isEmpty(),"Mobile value is Empty");
		String mpesa_agent_value = withdrawCashConfirmationPageObject.txt_dialog_amount.getText();
		Assert.assertEquals(false, mpesa_agent_value.isEmpty(),"Agent value is empty");
		String mpesa_amount_value = withdrawCashConfirmationPageObject.tv_mpesa_sendto_value.getText();
		Assert.assertEquals(false, mpesa_amount_value.isEmpty(),"Send to value is empty");
		Assert.assertEquals(true, withdrawCashConfirmationPageObject.txt_continue_dilaog.isEnabled(),"Continue button is disabled");
		Assert.assertEquals(true, withdrawCashConfirmationPageObject.txt_cancel_dilaog.isEnabled(),"Cancel button is disabled");
		wait.until(ExpectedConditions.elementToBeClickable(withdrawCashConfirmationPageObject.txt_continue_dilaog))
				.click();
		Thread.sleep(5000);
		String expected_label = "Please wait to enter M-PESA PIN.";
		String actual_label = withdrawCashFinalConfirmationPageObject.final_confirmation_label.getText();
		System.out.println(actual_label);
		wait.until(ExpectedConditions.elementToBeClickable(withdrawCashFinalConfirmationPageObject.finalbutton_click))
				.click();
		Assert.assertEquals(actual_label, expected_label);
//		wait.until(ExpectedConditions.elementToBeClickable(withdrawCashPageObject.backclick)).click();

	}

	/*
	 * Verify whether the "Withdraw From ATM" transaction is getting completed using
	 * valid ATM number
	 */

	@Test
	public void WITHDRAWCASH_TC_003() throws InterruptedException, IOException {

		
			wait.until(ExpectedConditions.elementToBeClickable(dphomePageObject.mpesa_click)).click();
			wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.withdraCashClick)).click();
		
			wait.until(ExpectedConditions.elementToBeClickable(withdrawCashPageObject.withdrawFromATM_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(withdrawCashPageObject.edt_agentnumber_WFATM))
				.sendKeys("1234");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(withdrawCashPageObject.WFATMContinueClick)).click();
		Thread.sleep(10000);
		String expected_confirmation_title = "CONFIRM";
		String actual_confirmation_title = withdrawCashConfirmationPageObject.confirmation_title.getText();
		Assert.assertEquals(actual_confirmation_title, expected_confirmation_title);
		String txt_business_val_atm = withdrawCashConfirmationPageObject.txt_business_val_atm.getText();
		Assert.assertEquals(false, txt_business_val_atm.isEmpty(),"Businees value is empty");
		Assert.assertEquals(true, withdrawCashConfirmationPageObject.txt_continue_dilaog.isEnabled(),"Continue button is disabled");
		Assert.assertEquals(true, withdrawCashConfirmationPageObject.txt_cancel_dilaog.isEnabled(),"Cancel button is disabled");
		wait.until(ExpectedConditions.elementToBeClickable(withdrawCashConfirmationPageObject.txt_continue_dilaog))
				.click();
		Thread.sleep(1000);
		String expected_label = "Please wait to enter M-PESA PIN.";
		String actual_label = withdrawCashFinalConfirmationPageObject.final_confirmation_label.getText();
		System.out.println(actual_label);
		wait.until(ExpectedConditions.elementToBeClickable(withdrawCashFinalConfirmationPageObject.finalbutton_click))
				.click();
		Assert.assertEquals(actual_label, expected_label);

	}

}
