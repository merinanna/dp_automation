package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.dphome.dpHomePage;
import com.safaricom.pages.menu.Menu_services;
import com.safaricom.pages.menu.Menudrawer;
import com.safaricom.pages.services.SkizaservicesConfirmationPage;
import com.safaricom.pages.services.SkizaservicesPage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class SkizaServicesTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public dpHomePage dphomepageObject=new dpHomePage(driver);
	public Menudrawer menudrawerObject=new Menudrawer(driver);
	public Menu_services menuservicesObject=new Menu_services(driver);	
	public SkizaservicesPage skizaservicesPageObject=new SkizaservicesPage(driver);
	public SkizaservicesConfirmationPage skizaservicesConfirmationPage=new SkizaservicesConfirmationPage(driver);
	

	/* Verify whether there is option to search Skiza tunes with Artist and Song and the title should be "SKIZA SERVICES". */
	
	@Test
	public void MENU_SKIZASERVICES_TC_001() throws InterruptedException, IOException {
	
		wait.until(ExpectedConditions.elementToBeClickable(dphomepageObject.menu_expand)).click();
		wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.services_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(menuservicesObject.skizaservices_click)).click();
		String expected_maintitle = "SKIZA SERVICES";
		String actual_maintitle = skizaservicesPageObject.skizaservices_title.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		
		Assert.assertEquals(true, skizaservicesPageObject.rbartist_click.isDisplayed(), "Artist Radio button is not displayed");
		Assert.assertEquals(true, skizaservicesPageObject.rbsong_click.isDisplayed(), "Song Radio button is not displayed");
		Assert.assertEquals(true, skizaservicesPageObject.artist_song.isDisplayed(), "Artist/Song field is not displayed");
		Assert.assertEquals(true, skizaservicesPageObject.search_btn_Click.isDisplayed(), "Search button is not displayed");
		
		wait.until(ExpectedConditions.elementToBeClickable(skizaservicesPageObject.rbartist_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(skizaservicesPageObject.artist_song)).sendKeys("A R RAHMAN");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(skizaservicesPageObject.search_btn_Click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(skizaservicesConfirmationPage.tv_ok)).click();
		
	}

}
