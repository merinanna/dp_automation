package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.dphome.MpesaHomePage;
import com.safaricom.pages.dphome.dpHomePage;
import com.safaricom.pages.dphome.sfcHomePage;
import com.safaricom.pages.mpesa.BuyGoodsandServicesFinalConfirmation;
import com.safaricom.pages.mpesa.BuyGoodsandServicesPage;
import com.safaricom.pages.mpesa.BuyGoodsandServicesSendClick;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class BuyGoodsAndServicesTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public sfcHomePage sfcHomePageObject = new sfcHomePage(driver);
	public MpesaHomePage mpesaHomePageObject = new MpesaHomePage(driver);
	public dpHomePage dpHomeObject=new dpHomePage(driver);
	public BuyGoodsandServicesPage buyGoodsandServicesPageObject = new BuyGoodsandServicesPage(driver);
	public BuyGoodsandServicesSendClick buyGoodsandServicesSendClickObject = new BuyGoodsandServicesSendClick(driver);
	public BuyGoodsandServicesFinalConfirmation buyGoodsandServicesFinalConfirmationObject = new BuyGoodsandServicesFinalConfirmation(
			driver);


	@Test
	public void BUYGOODSNSERVICES_TC_001() throws InterruptedException, IOException {
		
		wait.until(ExpectedConditions.elementToBeClickable(dpHomeObject.buyGoods_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(buyGoodsandServicesPageObject.BuygoodsClick)).click();
		wait.until(ExpectedConditions.elementToBeClickable(buyGoodsandServicesPageObject.Buygoods_edt_till_no)).sendKeys("12122");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(buyGoodsandServicesPageObject.Buygoods_edt_till_amount)).sendKeys("200");
		driver.navigate().back();
//		PointOption startPoint = PointOption.point(360, 1583);
//		PointOption endPoint = PointOption.point(360, 859);
//		new TouchAction(driver).press(startPoint).moveTo(endPoint).release().perform();
		wait.until(ExpectedConditions.elementToBeClickable(buyGoodsandServicesPageObject.Buygoods_continue)).click();
		Thread.sleep(10000);
		String expected_confirmation_title_buygoods = "CONFIRM";
		String actual_confirmation_title_buygoods = buyGoodsandServicesSendClickObject.lipanampesa_confirmation_title.getText();
		Assert.assertEquals(actual_confirmation_title_buygoods, expected_confirmation_title_buygoods);
		String txt_business_buygoods = buyGoodsandServicesSendClickObject.txt_business_val_.getText();
		Assert.assertEquals(false, txt_business_buygoods.isEmpty(),"txt_business_buygoods is empty");
		String txt_dialog_amount_buygoods = buyGoodsandServicesSendClickObject.txt_dialog_amount.getText();
		Assert.assertEquals(false, txt_dialog_amount_buygoods.isEmpty(),"txt_dialog_amount_buygoods is empty");
		String tv_mpesa_mobile_value_buygoods = buyGoodsandServicesSendClickObject.tv_mpesa_mobile_value.getText();
		Assert.assertEquals(false, tv_mpesa_mobile_value_buygoods.isEmpty(),"tv_mpesa_mobile_value_buygoods is empty");
		Assert.assertEquals(true, buyGoodsandServicesSendClickObject.txt_cancel_dilaog.isEnabled(),"Cancel button is disabled");
		Assert.assertEquals(true, buyGoodsandServicesSendClickObject.txt_continue_dilaog.isEnabled(),"Continue button is disabled");
		wait.until(ExpectedConditions.elementToBeClickable(buyGoodsandServicesSendClickObject.txt_continue_dilaog)).click();
		Thread.sleep(1000);
		String expected_label = "Please wait to enter M-PESA PIN.";
		String actual_label = buyGoodsandServicesFinalConfirmationObject.success_message.getText();
		System.out.println(actual_label);
		wait.until(ExpectedConditions.elementToBeClickable(buyGoodsandServicesFinalConfirmationObject.btn_close)).click();
		Assert.assertEquals(actual_label, expected_label);

	}

}
