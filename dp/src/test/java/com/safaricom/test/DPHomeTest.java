package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.dphome.dpHomePage;
import com.safaricom.pages.dphome.sfcHomePage;
import com.safaricom.pages.login.LocationPermissionPage;
import com.safaricom.pages.login.LoginFailurePage;
import com.safaricom.pages.login.LoginGeneratePinPage;
import com.safaricom.pages.login.LoginSuccessPage;
import com.safaricom.pages.login.ManagePhonePermissionPage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class DPHomeTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public LocationPermissionPage locationPermissionObject = new LocationPermissionPage(driver);
	public ManagePhonePermissionPage managePhonePermissionObject = new ManagePhonePermissionPage(driver);
	public LoginGeneratePinPage loginGeneratePinObject = new LoginGeneratePinPage(driver);
	public LoginSuccessPage loginSuccessPageObject = new LoginSuccessPage(driver);
	public dpHomePage dpHomePageObject = new dpHomePage(driver);
	public LoginFailurePage loginfailureObject = new LoginFailurePage(driver);
	public sfcHomePage sfchomepageObject=new sfcHomePage(driver);
	public MenuTest menutestObject=new MenuTest();

	/*
	 * Verify whether the icons such as
	 * "CHECKBALANCE,M-PESA,TUNUKIWA,MY DATA USAGE,DATA AND SMS PLANS,PLATINUM PLANS,SERVICES,MY ACCOUNT AND CONTACT US "
	 * and the "BANNER" is displayed in SFC Home Page and Title should be "MENU"
	 */
	@Test
	public void DP_TC_001() throws InterruptedException, IOException {

		Thread.sleep(10000);
 		Assert.assertEquals(true, dpHomePageObject.menu_expand.isDisplayed());
		Assert.assertEquals(true, dpHomePageObject.iv_profile_image.isDisplayed());
		Assert.assertEquals(true, dpHomePageObject.greeting_msg.isDisplayed());
		Assert.assertEquals(true, dpHomePageObject.FirstName.isDisplayed());
		Assert.assertEquals(true, dpHomePageObject.plan_balance.isDisplayed());
		Assert.assertEquals(true, dpHomePageObject.notification_click.isDisplayed());
		Assert.assertEquals(true, dpHomePageObject.deals_click.isDisplayed());
		Assert.assertEquals(true, dpHomePageObject.active_bills.isDisplayed());
		Assert.assertEquals(true, dpHomePageObject.account_click.isDisplayed());
		Assert.assertEquals(true, dpHomePageObject.buy_plan.isDisplayed());
		
		Assert.assertEquals(true, dpHomePageObject.my_bills.isDisplayed());
		Assert.assertEquals(true, dpHomePageObject.paybill_labelTitle.isDisplayed());
		Assert.assertEquals(true, dpHomePageObject.pay_button.isDisplayed());
		Assert.assertEquals(true, dpHomePageObject.mpesa_click.isDisplayed());
		Assert.assertEquals(true, dpHomePageObject.sendMoney_click.isDisplayed());
		Assert.assertEquals(true, dpHomePageObject.buyGoods_click.isDisplayed());
		Assert.assertEquals(true, dpHomePageObject.myDataUsage_click.isDisplayed());
		
	}

}
