package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.dphome.AccountHomePage;
import com.safaricom.pages.dphome.MpesaHomePage;
import com.safaricom.pages.dphome.dpHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class MpesaHomeTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public dpHomePage dphomepageObject=new dpHomePage(driver);
	public AccountHomePage accounthomepageObject=new AccountHomePage(driver);
	public MpesaHomePage mpesahomepageObject=new MpesaHomePage(driver);
	
	
	@Test
	public void MPESA_TC_001() throws InterruptedException, IOException {
		
		Thread.sleep(10000);
		wait.until(ExpectedConditions.elementToBeClickable(dphomepageObject.mpesa_click)).click();
		String expectedtitle ="M-PESA";
		String sfctitle = mpesahomepageObject.mpesa_title.getText();Assert.assertEquals(true, mpesahomepageObject.mpesa_show_balance.isDisplayed());
//		driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.selfcare.safaricom:id/scroll\")).scrollIntoView("
//				+ "new UiSelector().text(\"SEND MONEY\"))");
//		data_click.click();

		Assert.assertEquals(sfctitle, expectedtitle);
		
		Assert.assertEquals(true, mpesahomepageObject.sendMoneyClick.isDisplayed());
		Assert.assertEquals(true, mpesahomepageObject.withdraCashClick.isDisplayed());
		Assert.assertEquals(true, mpesahomepageObject.buyairtimeClick.isDisplayed());
		Assert.assertEquals(true, mpesahomepageObject.lipanampesaClick.isDisplayed());
		Assert.assertEquals(true, mpesahomepageObject.loansandsavingsClick.isDisplayed());
		Assert.assertEquals(true, mpesahomepageObject.myaccountClick.isDisplayed());
	//	Assert.assertEquals(true, mpesahomepageObject.fulizampesaClick.isDisplayed());
		Assert.assertEquals(true, mpesahomepageObject.mpesaglobalClick.isDisplayed());
		driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.selfcare.safaricom:id/scroll\")).scrollIntoView("
				+ "new UiSelector().text(\"Scan QR\"))");	
		Assert.assertEquals(true, mpesahomepageObject.mpesa_buttonScanQR.isDisplayed());
		driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.selfcare.safaricom:id/scroll\")).scrollIntoView("
				+ "new UiSelector().text(\"M-PESA Statement\"))");	
		Assert.assertEquals(true, mpesahomepageObject.mpesa_statement.isDisplayed());
		
	}
}