package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.MyBill.ErrorPage;
import com.safaricom.pages.MyBill.PrepaidUsageStatementFullPage;
import com.safaricom.pages.MyBill.PrepaidUsageStatementMiniPage;
import com.safaricom.pages.MyBill.RequestForBillConfPage;
import com.safaricom.pages.MyBill.RequestForBillPage;
import com.safaricom.pages.MyBill.TransactionCodePage;
import com.safaricom.pages.backtoplatinum.Menu_backtoplatinum;
import com.safaricom.pages.backtoplatinum.Menu_backtoplatinumfinalConfirmation;
import com.safaricom.pages.contactus.ContactUSHomePage;
import com.safaricom.pages.contactus.ContactUS_NewRequest;
import com.safaricom.pages.contactus.ContactUS_NewRequestFinalConfirmation;
import com.safaricom.pages.dphome.dpHomePage;
import com.safaricom.pages.dphome.sfcHomePage;
import com.safaricom.pages.menu.DateSelector;
import com.safaricom.pages.menu.MenuDataUsagePage;
import com.safaricom.pages.menu.MenuMyBillHomePage;
import com.safaricom.pages.menu.MenuMyaccountHomePage;
import com.safaricom.pages.menu.Menu_LoginPage;
import com.safaricom.pages.menu.Menu_LogoutConfirmation;
import com.safaricom.pages.menu.Menu_SetServicePinConf;
import com.safaricom.pages.menu.Menu_SetServicePinFinalConf;
import com.safaricom.pages.menu.Menu_SettingsChangeServicePin;
import com.safaricom.pages.menu.Menu_SettingsChangeServicePinConf;
import com.safaricom.pages.menu.Menu_SettingsSetServicePin;
import com.safaricom.pages.menu.Menu_backtoSafaricomApp;
import com.safaricom.pages.menu.Menu_backtoSafaricomAppConfirmation;
import com.safaricom.pages.menu.Menu_feedback;
import com.safaricom.pages.menu.Menu_feedbackConfirmation;
import com.safaricom.pages.menu.Menu_knowledgebase;
import com.safaricom.pages.menu.Menu_services;
import com.safaricom.pages.menu.Menu_setting;
import com.safaricom.pages.menu.Menu_storelocator;
import com.safaricom.pages.menu.Menu_tellafriend;
import com.safaricom.pages.menu.Menudrawer;
import com.safaricom.pages.myaccount.MyAccount_BalanceHomePage;
import com.safaricom.pages.myaccount.MyAccount_PukHomePage;
import com.safaricom.pages.myaccount.MyAccount_TopupHomePage;
import com.safaricom.pages.services.DSPlanActivateConfirmationPage;
import com.safaricom.pages.services.DSPlanActivateFinalConfirmationPage;
import com.safaricom.pages.services.DataandsmsplanservicesPage;
import com.safaricom.pages.services.DateSelectPage;
import com.safaricom.pages.services.RedeemBongaPointConfirmationPage;
import com.safaricom.pages.services.RedeemBongaPointFinalConfirmationPage;
import com.safaricom.pages.services.RedeemBongaPointPage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class MyBillTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public dpHomePage dpHomePageObject=new dpHomePage(driver);
	public Menudrawer menudrawerObject=new Menudrawer(driver);
	public ContactUSHomePage contactUshomePageObject=new ContactUSHomePage(driver);
	public ContactUS_NewRequest contactUS_NewRequestObject=new ContactUS_NewRequest(driver);
	public RedeemBongaPointPage redeemBongaPointPageObject=new RedeemBongaPointPage(driver);
	public RedeemBongaPointConfirmationPage redeemBongaPointConfirmationPageObject=new RedeemBongaPointConfirmationPage(driver);
	public RedeemBongaPointFinalConfirmationPage redeemBongaPointFinalConfirmationPageObject=new RedeemBongaPointFinalConfirmationPage(driver);
	public ContactUS_NewRequestFinalConfirmation  contactUS_NewRequestFinalConfirmationObject=new ContactUS_NewRequestFinalConfirmation(driver);
	public Menu_services menuservicesObject=new Menu_services(driver);	
	public MenuMyaccountHomePage myaccounthomePageObject=new MenuMyaccountHomePage(driver);
	public DateSelector dateSelectorObject=new DateSelector();
	public MyAccount_BalanceHomePage myaccount_BalanceHomePageObject=new MyAccount_BalanceHomePage(driver);
	public MyAccount_PukHomePage myaccountpukhomepageObject=new MyAccount_PukHomePage(driver);
	public MyAccount_TopupHomePage myaccounttopuphomepageObject=new MyAccount_TopupHomePage(driver);
	public Menu_knowledgebase menuknowledgebaseObject=new Menu_knowledgebase(driver);
	public Menu_storelocator menustorelocatorObject=new Menu_storelocator(driver);
	public Menu_tellafriend menutellafriendObject=new Menu_tellafriend(driver);
	public Menu_feedback  menufeedbackObject=new Menu_feedback(driver);
	public Menu_feedbackConfirmation menufeedbackconfirmationObject=new Menu_feedbackConfirmation(driver);
	public sfcHomePage sfcHomePageObject=new sfcHomePage(driver);
	public MenuDataUsagePage menuDataUsagePageObject=new MenuDataUsagePage(driver);
	public Menu_setting menusettingObject = new Menu_setting(driver);
	public Menu_SettingsChangeServicePin menusettingchangeservicepinObject=new Menu_SettingsChangeServicePin(driver);
	public Menu_SettingsChangeServicePinConf menusettingchangeservicepinconfObject =new Menu_SettingsChangeServicePinConf(driver);
	public Menu_SettingsSetServicePin menusetservicepinObject=new Menu_SettingsSetServicePin(driver);
	public Menu_SetServicePinConf menusetservicepinConfObject=new Menu_SetServicePinConf(driver);
	public Menu_SetServicePinFinalConf menusetservicepinfinalConfObject=new Menu_SetServicePinFinalConf(driver);
	public MenuMyBillHomePage menumyBillHomePageObject=new MenuMyBillHomePage(driver);
	public Menu_backtoSafaricomApp menu_backtoSafaricom=new Menu_backtoSafaricomApp(driver);
	public Menu_backtoSafaricomAppConfirmation menu_backtoSafaricomfinalConfirmationObject=new Menu_backtoSafaricomAppConfirmation(driver);
	public Menu_backtoplatinum menu_backtoplatinum=new Menu_backtoplatinum(driver);
	public Menu_backtoplatinumfinalConfirmation menu_backtoplatinumfinalConfirmationObject=new Menu_backtoplatinumfinalConfirmation(driver);
	public Menu_LogoutConfirmation menuLogoutConfirmationObject=new Menu_LogoutConfirmation(driver);
	public Menu_LoginPage menuLoginPageObject=new Menu_LoginPage(driver);
	public RequestForBillPage requestForBillPageObject=new RequestForBillPage(driver);
	public RequestForBillConfPage requestForBillConfPageObject=new RequestForBillConfPage(driver);
	public DataandsmsplanservicesPage dataandsmsplanservicesPageObject=new DataandsmsplanservicesPage(driver);
	public DSPlanActivateConfirmationPage dsplanActivateComfirmationPageObject=new  DSPlanActivateConfirmationPage(driver);
	public DSPlanActivateFinalConfirmationPage dsplanActivateFinalComfirmationPageObject = new DSPlanActivateFinalConfirmationPage(driver);
	public PrepaidUsageStatementFullPage prepaidUsageStatementFullPageObject=new PrepaidUsageStatementFullPage(driver);
	public PrepaidUsageStatementMiniPage prepaidUsageStatementMiniPageObject=new PrepaidUsageStatementMiniPage(driver);
    public DateSelectPage dateselectpageObject=new DateSelectPage(driver);
    public TransactionCodePage transactionPageObject=new TransactionCodePage(driver);
    public ErrorPage errorPageObject=new ErrorPage(driver);
	
	@Test
	public void MENU_MYBILLS_TC_001() throws InterruptedException, IOException {

	
		wait.until(ExpectedConditions.elementToBeClickable(dpHomePageObject.menu_expand)).click();
		MobileElement myBill = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.selfcare.safaricom:id/exp_account\")).scrollIntoView("
				+ "new UiSelector().text(\"MY BILL\"))");
		myBill.click();
		String expectedtitle = "MY BILLS";
		String actualtitle = menumyBillHomePageObject.myBill_title.getText();
		Assert.assertEquals(actualtitle, expectedtitle);
		Assert.assertEquals(true, menumyBillHomePageObject.tv_requestforbill.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(menumyBillHomePageObject.tv_requestforbill)).click();
		wait.until(ExpectedConditions.elementToBeClickable(requestForBillPageObject.email_address)).sendKeys("merinanna27@gmail.com");
		wait.until(ExpectedConditions.elementToBeClickable(requestForBillPageObject.service_pin)).sendKeys("1234");
		wait.until(ExpectedConditions.elementToBeClickable(requestForBillPageObject.btn_ok)).click();
		wait.until(ExpectedConditions.elementToBeClickable(requestForBillConfPageObject.ok_btn)).click();
			
	}
	
	@Test
	public void MENU_MYBILLS_TC_002() throws InterruptedException, IOException {

	
		wait.until(ExpectedConditions.elementToBeClickable(dpHomePageObject.menu_expand)).click();
		MobileElement myBill = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.selfcare.safaricom:id/exp_account\")).scrollIntoView("
				+ "new UiSelector().text(\"MY BILL\"))");
		myBill.click();
		String expectedtitle = "MY BILLS";
		String actualtitle = menumyBillHomePageObject.myBill_title.getText();
		Assert.assertEquals(actualtitle, expectedtitle);
		Assert.assertEquals(true, menumyBillHomePageObject.buttonPayTheBill.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(menumyBillHomePageObject.buttonPayTheBill)).click();
		
	}
	
	
	@Test
	public void MENU_MYBILLS_TC_003() throws InterruptedException, IOException {

	
		wait.until(ExpectedConditions.elementToBeClickable(dpHomePageObject.menu_expand)).click();
		MobileElement myBill = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.selfcare.safaricom:id/exp_account\")).scrollIntoView("
				+ "new UiSelector().text(\"MY BILL\"))");
		myBill.click();
		wait.until(ExpectedConditions.elementToBeClickable(menumyBillHomePageObject.btn_redeem_bonga)).click();
//		String expected_maintitle = "Bonga Points";
//		String actual_maintitle = redeemBongaPointPageObject.title_bonga_points.getText();
//		Assert.assertEquals(actual_maintitle, expected_maintitle);
		wait.until(ExpectedConditions.elementToBeClickable(redeemBongaPointPageObject.redeem_option)).click();
		wait.until(ExpectedConditions.elementToBeClickable(redeemBongaPointPageObject.redeem_option_select)).click();
		wait.until(ExpectedConditions.elementToBeClickable(redeemBongaPointPageObject.amount_field)).click();
		wait.until(ExpectedConditions.elementToBeClickable(redeemBongaPointPageObject.amount_field_select)).click();
		wait.until(ExpectedConditions.elementToBeClickable(redeemBongaPointPageObject.redeem_button)).click();
		wait.until(ExpectedConditions.elementToBeClickable(redeemBongaPointConfirmationPageObject.ok_click)).click();
		Thread.sleep(1000);
		String expected_confirmationmessage = "Please wait to enter M-PESA PIN.";
		String actual_confirmationmessage = redeemBongaPointFinalConfirmationPageObject.final_confirmation_message.getText();
		Assert.assertEquals(actual_confirmationmessage, expected_confirmationmessage);
		wait.until(ExpectedConditions.elementToBeClickable(redeemBongaPointFinalConfirmationPageObject.ok_click)).click();
		Thread.sleep(5000);
		
	}
	@Test
	public void MENU_MYBILLS_TC_004() throws InterruptedException, IOException {

	
		wait.until(ExpectedConditions.elementToBeClickable(dpHomePageObject.menu_expand)).click();
		MobileElement myBill = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.selfcare.safaricom:id/exp_account\")).scrollIntoView("
				+ "new UiSelector().text(\"MY BILL\"))");
		myBill.click();
		wait.until(ExpectedConditions.elementToBeClickable(menumyBillHomePageObject.btn_buy_data)).click();
		wait.until(ExpectedConditions.elementToBeClickable(dataandsmsplanservicesPageObject.activateClick)).click();
		Assert.assertEquals(true, dsplanActivateComfirmationPageObject.confirmation_message.isDisplayed(),"Confirmation Mesage is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateComfirmationPageObject.tv_ok)).click();
		Assert.assertEquals(true, dsplanActivateFinalComfirmationPageObject.finalconfirmation_message.isDisplayed(),"Final Confirmation Mesage is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(dsplanActivateFinalComfirmationPageObject.tv_ok)).click();
		
	}
	@Test
	public void MENU_MYBILLS_TC_005() throws InterruptedException, IOException {

	
		wait.until(ExpectedConditions.elementToBeClickable(dpHomePageObject.menu_expand)).click();
		MobileElement myBill = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.selfcare.safaricom:id/exp_account\")).scrollIntoView("
				+ "new UiSelector().text(\"MY BILL\"))");
		myBill.click();
		wait.until(ExpectedConditions.elementToBeClickable(menumyBillHomePageObject.buttonFullStatement)).click();
		String expected_maintitle = "Prepaid Usage Statement";
		String actual_maintitle = prepaidUsageStatementFullPageObject.prepaidUsageStatement_title.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		Assert.assertEquals(true, prepaidUsageStatementFullPageObject.inputEmail.isDisplayed(), "Email field is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(prepaidUsageStatementFullPageObject.inputEmail)).sendKeys("merin.ann@flytxt.com");
		wait.until(ExpectedConditions.elementToBeClickable(prepaidUsageStatementFullPageObject.labelFromDate)).click();
		wait.until(ExpectedConditions.elementToBeClickable(dateselectpageObject.ok_button)).click();
		wait.until(ExpectedConditions.elementToBeClickable(prepaidUsageStatementFullPageObject.labelToDate)).click();
		wait.until(ExpectedConditions.elementToBeClickable(dateselectpageObject.ok_button)).click();
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(prepaidUsageStatementFullPageObject.buttonPositive)).click();
		wait.until(ExpectedConditions.elementToBeClickable(transactionPageObject.inputTransactionCode)).sendKeys("1234");
		wait.until(ExpectedConditions.elementToBeClickable(transactionPageObject.submit_btn)).click();
	}
	
	@Test
	public void MENU_MYBILLS_TC_006() throws InterruptedException, IOException {

	
		wait.until(ExpectedConditions.elementToBeClickable(dpHomePageObject.menu_expand)).click();
		MobileElement myBill = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.selfcare.safaricom:id/exp_account\")).scrollIntoView("
				+ "new UiSelector().text(\"MY BILL\"))");
		myBill.click();
		wait.until(ExpectedConditions.elementToBeClickable(menumyBillHomePageObject.buttonMiniStatement)).click();
		wait.until(ExpectedConditions.elementToBeClickable(errorPageObject.ok_button)).click();
		
	}
	
	
}
