package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.dphome.dpHomePage;
import com.safaricom.pages.menu.Menu_services;
import com.safaricom.pages.menu.Menudrawer;
import com.safaricom.pages.services.OkoaServicesPage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class OkoaServicesTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public dpHomePage dphomepageObject=new dpHomePage(driver);
	public Menudrawer menudrawerObject=new Menudrawer(driver);
	public Menu_services menuservicesObject=new Menu_services(driver);	
	public OkoaServicesPage okoaServicePageObject=new OkoaServicesPage(driver);

	/* Verify whether Okoa Services details shown in Okoa Service home page and title should be "OKOA SERVICES" */
	
	@Test
	public void MENU_OKOASERVICES_TC_001() throws InterruptedException, IOException {

		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(dphomepageObject.menu_expand)).click();
		wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.services_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(menuservicesObject.okoaservices_click)).click();
		
		String expected_maintitle = "OKOA SERVICES";
		String actual_maintitle = okoaServicePageObject.okoaservices_title.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
	}

	
}
