package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.dphome.MpesaHomePage;
import com.safaricom.pages.dphome.dpHomePage;
import com.safaricom.pages.dphome.sfcHomePage;
import com.safaricom.pages.mpesa.SendMoneyConfirmationPage;
import com.safaricom.pages.mpesa.SendMoneyFinalConfirmationPage;
import com.safaricom.pages.mpesa.SendMoneyPage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class SendMoneyTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public sfcHomePage sfcHomePageObject = new sfcHomePage(driver);
	public MpesaHomePage mpesaHomePageObject = new MpesaHomePage(driver);
	public dpHomePage dphomePageObject=new dpHomePage(driver);
	public SendMoneyPage sendMoneyPageObject = new SendMoneyPage(driver);
	public SendMoneyConfirmationPage sendMoneySendClickObject = new SendMoneyConfirmationPage(driver);
	public SendMoneyFinalConfirmationPage sendMoneyFinalConfirmationPageObject = new SendMoneyFinalConfirmationPage(
			driver);

	/*
	 * Verfy whether the elements "SEND TO ONE,SEND TO OTHER NETWORK" is present in
	 * Send Money Home Page and Title should be "SEND MONEY"
	 */

	@Test
	public void SENDMONEY_TC_001() throws InterruptedException, IOException {

		
		
		wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.sendMoneyClick)).click();
		String expectedtitle = "SEND MONEY";
		String sendmoney_title = sendMoneyPageObject.SendMoney_title.getText();
		System.out.println(sendmoney_title);
		Assert.assertEquals(sendmoney_title, expectedtitle);
		Assert.assertEquals(true, sendMoneyPageObject.SendtoOnePhoneNumber.isDisplayed(),"Send to One Phone Number is not displayed");
		Assert.assertEquals(true, sendMoneyPageObject.SendtoOnePhoneAmount.isDisplayed(),"Send to One Amount is not displayed");
		Assert.assertEquals(true, sendMoneyPageObject.SendtoOneContinue.isDisplayed(),"Send to One continue button is not displayed");
		Assert.assertEquals(true, sendMoneyPageObject.SendtoOtherClick.isDisplayed(),"Send ot Other is not displayed");
		wait.until(ExpectedConditions.elementToBeClickable(sendMoneyPageObject.SendtoOtherClick)).click();
		Assert.assertEquals(true, sendMoneyPageObject.SendtoOtherPhoneNumber.isDisplayed(),"Send to Other Phone number is not displayed");
		Assert.assertEquals(true, sendMoneyPageObject.SendtoOtherPhoneAmount.isDisplayed(),"Send to Other Phone amount is not displayed");
		Assert.assertEquals(true, sendMoneyPageObject.SendToOtherContinue.isDisplayed(),"Send to Other Continue is not displayed");

	}

	/*
	 * Verify whether the "Send To One" transaction is getting completed using valid
	 * Phone number and valid Amount
	 */

	@Test
	public void SENDMONEY_TC_002() throws InterruptedException, IOException {
		
						wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.sendMoneyClick)).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(sendMoneyPageObject.SendtoOneClick)).click();
		wait.until(ExpectedConditions.elementToBeClickable(sendMoneyPageObject.SendtoOnePhoneNumber))
				.sendKeys("790771777");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(sendMoneyPageObject.SendtoOnePhoneAmount)).sendKeys("200");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(sendMoneyPageObject.SendtoOneContinue)).click();
		Thread.sleep(50000);
		String expected_confirmation_title ="Confirm";
		String actual_confirmation_title = sendMoneySendClickObject.confirmation_title.getText();
		System.out.println(actual_confirmation_title);
		Assert.assertEquals(actual_confirmation_title, expected_confirmation_title);
		String mpesa_mobile_value = sendMoneySendClickObject.conf_value.get(1).getText();
		Assert.assertEquals(false, mpesa_mobile_value.isEmpty(),"Mobile vaue is empty");
		String mpesa_amount_value = sendMoneySendClickObject.conf_value.get(2).getText();
		Assert.assertEquals(false, mpesa_amount_value.isEmpty(),"Amount value is empty");
		String transaction_cost = sendMoneySendClickObject.conf_value.get(3).getText();
		Assert.assertEquals(false, transaction_cost.isEmpty(),"Transaction Cost value is empty");
		Assert.assertEquals(true, sendMoneySendClickObject.sendmoney_cancel.isEnabled(),"Cancel button is disabled");
		Assert.assertEquals(true, sendMoneySendClickObject.sendmoney_continue.isEnabled(),"Continue button is displayed");
		wait.until(ExpectedConditions.elementToBeClickable(sendMoneySendClickObject.sendmoney_continue)).click();
		Thread.sleep(5000);
		String expected_label = "Please wait to enter M-PESA PIN.";
		String actual_label = sendMoneyFinalConfirmationPageObject.final_confirmation_label.getText();
		System.out.println(actual_label);
		wait.until(ExpectedConditions
				.elementToBeClickable(sendMoneyFinalConfirmationPageObject.Sendmoney_ok_click)).click();
		Assert.assertEquals(actual_label, expected_label);
	}

	/*
	 * Verify whether the "Send To Other Network" transaction is getting completed
	 * using valid Phone number and valid Amount
	 */

	@Test
	public void SENDMONEY_TC_003() throws InterruptedException, IOException {
		
					wait.until(ExpectedConditions.elementToBeClickable(mpesaHomePageObject.sendMoneyClick)).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(sendMoneyPageObject.SendtoOtherClick)).click();
		wait.until(ExpectedConditions.elementToBeClickable(sendMoneyPageObject.SendtoOtherPhoneNumber))
				.sendKeys("790771777");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(sendMoneyPageObject.SendtoOtherPhoneAmount)).sendKeys("200");
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(sendMoneyPageObject.SendToOtherContinue)).click();
		Thread.sleep(5000);
		wait.until(ExpectedConditions.elementToBeClickable(sendMoneyPageObject.errormsg_close)).click();
		driver.navigate().back();
//		wait.until(ExpectedConditions.elementToBeClickable(sendMoneyPageObject.backclick)).click(); // Click back button
//		String expected_confirmation_title_other ="CONFIRM";
//		String actual_confirmation_title_other = sendMoneySendClickObject.confirmation_title.getText();
//		Assert.assertEquals(actual_confirmation_title_other, expected_confirmation_title_other);
//		String mpesa_mobile_value = sendMoneySendClickObject.conf_value.get(1).getText();
//		Assert.assertEquals(false, mpesa_mobile_value.isEmpty(),"Mobile vaue is empty");
//		String mpesa_amount_value = sendMoneySendClickObject.conf_value.get(2).getText();
//		Assert.assertEquals(false, mpesa_amount_value.isEmpty(),"Amount value is empty");
//		String transaction_cost = sendMoneySendClickObject.conf_value.get(3).getText();
//		Assert.assertEquals(false, transaction_cost.isEmpty(),"Transaction Cost value is empty");
//		Assert.assertEquals(true, sendMoneySendClickObject.sendmoney_cancel.isEnabled(),"Cancel  button is disabled");
//		Assert.assertEquals(true, sendMoneySendClickObject.sendmoney_continue.isEnabled(),"Continue button is disabled");
//		wait.until(ExpectedConditions.elementToBeClickable(sendMoneySendClickObject.sendmoney_continue)).click();
//		Thread.sleep(5000);
//		String expected_label_other = "Please wait to enter M-PESA PIN.";
//		String actual_label_other = sendMoneyFinalConfirmationPageObject.final_confirmation_label.getText();
//		Assert.assertEquals(actual_label_other, expected_label_other);

	}

}
