package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.dphome.MpesaHomePage;
import com.safaricom.pages.dphome.dpHomePage;
import com.safaricom.pages.mpesa.FulizaMPesaConfirmationPage;
import com.safaricom.pages.mpesa.FulizaMPesaPage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class FulizampesaTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public MpesaHomePage mpesahomepageObject = new MpesaHomePage(driver);
	public FulizaMPesaPage fulizapageObject = new FulizaMPesaPage(driver);
	public FulizaMPesaConfirmationPage fulizaconfirmationpageObject = new FulizaMPesaConfirmationPage(driver);
	public dpHomePage dphomePageObject = new dpHomePage(driver);

	/*
	 * Verify whether user is able to Opt in to Fuliza M-Pesa and the title should
	 * be "FULIZA M-PESA"
	 */

	@Test
	public void FULIZAMPESA_TC_001() throws InterruptedException, IOException {

		wait.until(ExpectedConditions.elementToBeClickable(dphomePageObject.mpesa_click)).click();
		Thread.sleep(5000);
		wait.until(ExpectedConditions.elementToBeClickable(mpesahomepageObject.fulizampesaClick)).click();
		Thread.sleep(5000);
		String expectedtitle = "FULIZA M-PESA";
		String fuliza_title = fulizapageObject.fuliza_title.getText();
		Assert.assertEquals(fuliza_title, expectedtitle);
		driver.findElementByAndroidUIAutomator(
				"new UiScrollable(new UiSelector().className(\"android.widget.ScrollView\")).scrollIntoView("
						+ "new UiSelector().text(\"Accept the Terms & Conditions\"))");

		wait.until(ExpectedConditions.elementToBeClickable(fulizapageObject.fulizacheckbox_Click)).click();

		driver.findElementByAndroidUIAutomator(
				"new UiScrollable(new UiSelector().className(\"android.widget.ScrollView\")).scrollIntoView("
						+ "new UiSelector().text(\"OPT IN\"))");

		wait.until(ExpectedConditions.elementToBeClickable(fulizapageObject.fulizaoptin_continue)).click();
		wait.until(ExpectedConditions.elementToBeClickable(fulizaconfirmationpageObject.fuliza_optin_ok)).click();
			
	}

}
