package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.dphome.AccountHomePage;
import com.safaricom.pages.dphome.dpHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class AccountTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public dpHomePage dphomepageObject=new dpHomePage(driver);
	public AccountHomePage accounthomepageObject=new AccountHomePage(driver);
	
	
	@Test
	public void ACCOUNT_TC_001() throws InterruptedException, IOException {
		
		wait.until(ExpectedConditions.elementToBeClickable(dphomepageObject.account_click)).click();

		MobileElement data_click = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.selfcare.safaricom:id/exp_account\")).scrollIntoView("
				+ "new UiSelector().text(\"Data\"))");
		data_click.click();
		
		Assert.assertEquals(true, accounthomepageObject.data_limited_amount.isDisplayed());
		Assert.assertEquals(true, accounthomepageObject.data_expirydate.isDisplayed());
		
		MobileElement sms_click = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.selfcare.safaricom:id/exp_account\")).scrollIntoView("
				+ "new UiSelector().text(\"SMS\"))");
		sms_click.click();
		
		Assert.assertEquals(true, accounthomepageObject.sms_amount.isDisplayed());
				
		MobileElement voice_Click = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.selfcare.safaricom:id/exp_account\")).scrollIntoView("
				+ "new UiSelector().text(\"Voice\"))");
		voice_Click.click();
		
		Assert.assertEquals(true, accounthomepageObject.onNettalktime.isDisplayed());
			
		MobileElement airtime_click = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.selfcare.safaricom:id/exp_account\")).scrollIntoView("
				+ "new UiSelector().text(\"Airtime\"))");
		airtime_click.click();
		
		Assert.assertEquals(true, accounthomepageObject.airtime_prepaid_balance.isDisplayed());
		Assert.assertEquals(true, accounthomepageObject.airtime_expirydate.isDisplayed());
		
		MobileElement bonga_click = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.selfcare.safaricom:id/exp_account\")).scrollIntoView("
				+ "new UiSelector().text(\"Bonga\"))");
		bonga_click.click();
		
		Assert.assertEquals(true, accounthomepageObject.bonga_balance_amount.isDisplayed());
		
		MobileElement okoa_click = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"com.selfcare.safaricom:id/exp_account\")).scrollIntoView("
				+ "new UiSelector().text(\"Okoa\"))");
		okoa_click.click();
		
		Assert.assertEquals(true, accounthomepageObject.okoa_airtime_balance.isDisplayed());
		Assert.assertEquals(true, accounthomepageObject.okoa_data_balance.isDisplayed());
		
		
	}
}
