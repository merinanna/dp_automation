package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.bills.BillManagerConfirmationPage;
import com.safaricom.pages.bills.BillManagerFinalConfirmationPage;
import com.safaricom.pages.bills.BillManagerPage;
import com.safaricom.pages.bills.billHomePage;
import com.safaricom.pages.bills.payBillPageFinalConfirmation;
import com.safaricom.pages.bills.payBillSendClick;
import com.safaricom.pages.bills.paybillHomePage;
import com.safaricom.pages.dphome.dpHomePage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class BillTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public dpHomePage dphomepageObject=new dpHomePage(driver);
	public billHomePage billHomePageObject=new billHomePage(driver);
	public paybillHomePage paybillPageObject = new paybillHomePage(driver);
	public payBillSendClick payBillSendClickObject = new payBillSendClick(driver);
	public payBillPageFinalConfirmation payBillPageFinalConfirmationObject = new payBillPageFinalConfirmation(driver);
	public BillManagerPage billmanagerPageObject = new BillManagerPage(driver);
	public BillManagerConfirmationPage billmanagerConfirmationPageObject = new BillManagerConfirmationPage(driver);
	public BillManagerFinalConfirmationPage billmanagerFinalConfirmationPageObject = new BillManagerFinalConfirmationPage(
			driver);
	
	
	
	@Test
	public void BILL_TC_001() throws InterruptedException, IOException {
		
		wait.until(ExpectedConditions.elementToBeClickable(dphomepageObject.active_bills)).click();

		MobileElement billManager = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.ScrollView\")).scrollIntoView("
				+ "new UiSelector().text(\"BILL MANAGER\"))");
	
		Assert.assertEquals(true, billHomePageObject.pay_button.isDisplayed());
		Assert.assertEquals(true, billManager.isDisplayed());
		
	}

	
	@Test
	public void BILL_TC_002() throws InterruptedException, IOException {
		
		
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(dphomepageObject.active_bills)).click();

		
			Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(billHomePageObject.pay_button)).click();
//			wait.until(ExpectedConditions.elementToBeClickable(paybillPageObject.paybillClick)).click();

if(paybillPageObject.paybill_edt_buss_no.getText().isEmpty())
{
	wait.until(ExpectedConditions.elementToBeClickable(paybillPageObject.paybill_edt_buss_no)).sendKeys("12345");
	driver.navigate().back();
}if(paybillPageObject.paybill_edt_account_number.getText().isEmpty())
{
		
		wait.until(ExpectedConditions.elementToBeClickable(paybillPageObject.paybill_edt_account_number)).sendKeys("790771777");
		driver.navigate().back();
}
if(paybillPageObject.paybill_edt_bill_amount.getText().isEmpty())
{

		wait.until(ExpectedConditions.elementToBeClickable(paybillPageObject.paybill_edt_bill_amount)).sendKeys("200");
		driver.navigate().back();
}
MobileElement paybill_button = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.LinearLayout\")).scrollIntoView("
		+ "new UiSelector().text(\"NEXT\"))");
paybill_button.click();
		//wait.until(ExpectedConditions.elementToBeClickable(paybillPageObject.paybill_continue)).click();
		Thread.sleep(10000);
		String expected_confirmation_title_paybill = "CONFIRM";
		String actual_confirmation_title_paybill = payBillSendClickObject.paybill_confirmation_title.getText();
		Assert.assertEquals(actual_confirmation_title_paybill, expected_confirmation_title_paybill);
		String txt_business_paybill = payBillSendClickObject.txt_business_val_.getText();
		Assert.assertEquals(false, txt_business_paybill.isEmpty(),"Business is empty ");
		String txt_dialog_account_number_paybill = payBillSendClickObject.txt_dialog_account_number.getText();
		Assert.assertEquals(false, txt_dialog_account_number_paybill.isEmpty(),"Account number is empty");
		String txt_dialog_amount_paybill = payBillSendClickObject.txt_dialog_amount.getText();
		Assert.assertEquals(false, txt_dialog_amount_paybill.isEmpty(),"Amount is empty");
		String tv_mpesa_mobile_value_paybill = payBillSendClickObject.tv_mpesa_mobile_value.getText();
		Assert.assertEquals(false, tv_mpesa_mobile_value_paybill.isEmpty(),"Mobile value is empty");
		Assert.assertEquals(true, payBillSendClickObject.txt_cancel_dilaog.isEnabled(),"Cancel button is disbled");
		Assert.assertEquals(true, payBillSendClickObject.txt_continue_dilaog.isEnabled(),"Continue button is disabled");
		wait.until(ExpectedConditions.elementToBeClickable(payBillSendClickObject.txt_continue_dilaog)).click();
		Thread.sleep(5000);
		String expected_label = "Please wait to enter M-PESA PIN.";
		String actual_label = payBillPageFinalConfirmationObject.success_message.getText();
		System.out.println(actual_label);
		wait.until(ExpectedConditions.elementToBeClickable(payBillPageFinalConfirmationObject.btn_close)).click();
		Assert.assertEquals(actual_label, expected_label);
		
	}
	
	@Test
	public void BILL_TC_003() throws InterruptedException, IOException {
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(dphomepageObject.active_bills)).click();
		MobileElement billManager = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.ScrollView\")).scrollIntoView("
				+ "new UiSelector().text(\"BILL MANAGER\"))");
		billManager.click();
		String expectedtitle = "MY BILLS";
		String billmanager_title = billmanagerPageObject.billmanager_title.getText();
		System.out.println(billmanager_title);
		Assert.assertEquals(billmanager_title, expectedtitle);
		wait.until(ExpectedConditions.elementToBeClickable(billmanagerPageObject.managebilltab_Click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(billmanagerPageObject.managebill_imgExpand)).click();
		Thread.sleep(1000);
		Assert.assertEquals(true, billmanagerPageObject.amount_field.isDisplayed(),"Amount Field is not displayed");
		Assert.assertEquals(true, billmanagerPageObject.managebill_paybutton.isDisplayed(),"Pay button is not displayed");
		
	}


@Test
public void BILL_TC_004() throws InterruptedException, IOException {
	
	wait.until(ExpectedConditions.elementToBeClickable(dphomepageObject.active_bills)).click();
	MobileElement billManager = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.ScrollView\")).scrollIntoView("
			+ "new UiSelector().text(\"BILL MANAGER\"))");
	billManager.click();
	wait.until(ExpectedConditions.elementToBeClickable(billmanagerPageObject.managebill_imgExpand)).click();
//	wait.until(ExpectedConditions.elementToBeClickable(billmanagerPageObject.amount_field)).click();
	if(billmanagerPageObject.amount_field.getText().isEmpty())
	{
		wait.until(ExpectedConditions.elementToBeClickable(billmanagerPageObject.amount_field)).sendKeys("200");
	}
	
	wait.until(ExpectedConditions.elementToBeClickable(billmanagerPageObject.managebill_paybutton)).click();
	Thread.sleep(1000);
	String expected_confirmation_title = "C O N F I R M";
	String actual_confirmation_title = billmanagerConfirmationPageObject.confirmation_title.getText();
	Assert.assertEquals(actual_confirmation_title, expected_confirmation_title);
	String txtpaybillnum = billmanagerConfirmationPageObject.txtpaybillnum.getText();
	Assert.assertEquals(false, txtpaybillnum.isEmpty(),"Paybill Number field id empty");
	String tvAccounttName = billmanagerConfirmationPageObject.tvAccounttName.getText();
	Assert.assertEquals(false, tvAccounttName.isEmpty(),"Account name field is empty");
	String tvAccountNo = billmanagerConfirmationPageObject.tvAccountNo.getText();
	Assert.assertEquals(false, tvAccountNo.isEmpty(),"Account number field is empty");
	String tv_amount = billmanagerConfirmationPageObject.tv_amount.getText();
	Assert.assertEquals(false, tv_amount.isEmpty(),"Amount field is empty");
	String tvTransactionCost = billmanagerConfirmationPageObject.tvTransactionCost.getText();
	Assert.assertEquals(false, tvTransactionCost.isEmpty(),"Transaction cost field is empty");
	Assert.assertEquals(true, billmanagerConfirmationPageObject.buttonNegative.isEnabled());
	Assert.assertEquals(true, billmanagerConfirmationPageObject.buttonPositive.isEnabled());
	wait.until(ExpectedConditions.elementToBeClickable(billmanagerConfirmationPageObject.buttonPositive)).click();
	Thread.sleep(5000);
	String expected_label = "Please wait to enter M-PESA PIN.";
	String actual_label = billmanagerFinalConfirmationPageObject.final_confirmation_label.getText();
	System.out.println(actual_label);
	wait.until(ExpectedConditions.elementToBeClickable(billmanagerFinalConfirmationPageObject.finalbutton_click))
			.click();
	Assert.assertEquals(actual_label, expected_label);
	wait.until(ExpectedConditions.elementToBeClickable(billmanagerPageObject.backclick)).click();

}

@Test
public void BILL_TC_005() throws InterruptedException, IOException {
	wait.until(ExpectedConditions.elementToBeClickable(dphomepageObject.active_bills)).click();
	MobileElement billManager = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.ScrollView\")).scrollIntoView("
			+ "new UiSelector().text(\"BILL MANAGER\"))");
	billManager.click();
	wait.until(ExpectedConditions.elementToBeClickable(billmanagerPageObject.frequentlypiadtab_Click)).click();
	Assert.assertEquals(true, billmanagerPageObject.searchfield.isDisplayed());
	
}

@Test
public void BILL_TC_006() throws InterruptedException, IOException {
	wait.until(ExpectedConditions.elementToBeClickable(dphomepageObject.active_bills)).click();
	MobileElement billManager = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.ScrollView\")).scrollIntoView("
			+ "new UiSelector().text(\"BILL MANAGER\"))");
	billManager.click();
	wait.until(ExpectedConditions.elementToBeClickable(billmanagerPageObject.popularbilltab_Click)).click();
	wait.until(ExpectedConditions.elementToBeClickable(billmanagerPageObject.popularbill_ivBill)).click();
	wait.until(ExpectedConditions.elementToBeClickable(billmanagerPageObject.popularbill_etAccountNo)).sendKeys("790771777");
	driver.navigate().back();
	wait.until(ExpectedConditions.elementToBeClickable(billmanagerPageObject.popularbill_continue)).click();
	wait.until(ExpectedConditions.elementToBeClickable(billmanagerFinalConfirmationPageObject.popularbill_confmsg)).click();
	wait.until(ExpectedConditions.elementToBeClickable(billmanagerFinalConfirmationPageObject.popularbill_okbtn)).click();
	
}






}