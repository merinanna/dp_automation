package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.dphome.dpHomePage;
import com.safaricom.pages.menu.Menu_services;
import com.safaricom.pages.menu.Menudrawer;
import com.safaricom.pages.menu.sfcHomePage;
import com.safaricom.pages.services.MysmsservicesDeactivateConfirmationPage;
import com.safaricom.pages.services.MysmsservicesDeactivateFinalConfirmationPage;
import com.safaricom.pages.services.MysmsservicesPage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;


@Test
public class MySMSServicesTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public Menudrawer menudrawerObject=new Menudrawer(driver);
	public Menu_services menuservicesObject=new Menu_services(driver);	
	public sfcHomePage sfcHomePageObject = new sfcHomePage(driver);
	public dpHomePage dphomepageObject=new dpHomePage(driver);
	public MysmsservicesPage mysmsservicesPageObject=new MysmsservicesPage(driver);
	public MysmsservicesDeactivateConfirmationPage mysmsservicesDeactivateConfirmationPageObject=new MysmsservicesDeactivateConfirmationPage(driver);
	public MysmsservicesDeactivateFinalConfirmationPage mysmsservicesDeactivateFinalConfirmationPageObject=new MysmsservicesDeactivateFinalConfirmationPage(driver);
	

	/*Verfiy whether Activated services are listed under "ACTIVATED"  tab and the title is "MY SMS SERVICES"*/
	
	@Test
	public void MENU_MYSMSSERVICES_TC_001() throws InterruptedException, IOException {

		Thread.sleep(1000); 
		wait.until(ExpectedConditions.elementToBeClickable(dphomepageObject.menu_expand)).click();
		wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.services_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(menuservicesObject.mysmsservices_click)).click();
		String expected_maintitle = "MY SMS SERVICES";
		String actual_maintitle = mysmsservicesPageObject.mysmsservices_title.getText();
		Assert.assertEquals(actual_maintitle, expected_maintitle);
		Assert.assertEquals(true, mysmsservicesPageObject.deactivateClick.isDisplayed(), "Deactivate is not displayed");
		
	}
	
	/*Verfiy whether user is able to deactivate activated service*/
	
	@Test
	public void MENU_MYSMSSERVICES_TC_002() throws InterruptedException, IOException {

		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(dphomepageObject.menu_expand)).click();
		wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.services_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(menuservicesObject.mysmsservices_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(mysmsservicesPageObject.deactivateClick)).click();
		wait.until(ExpectedConditions.elementToBeClickable(mysmsservicesDeactivateConfirmationPageObject.tv_ok)).click();
		wait.until(ExpectedConditions.elementToBeClickable(mysmsservicesDeactivateFinalConfirmationPageObject.tv_ok)).click();
	}

}
