package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.dphome.dpHomePage;
import com.safaricom.pages.menu.Menu_services;
import com.safaricom.pages.menu.Menudrawer;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class ServiceHomeTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public dpHomePage dpHomeObject=new dpHomePage(driver);
	public Menudrawer menudrawerObject=new Menudrawer(driver);
	public Menu_services menuservicesObject=new Menu_services(driver);	

	/*
	 * Verify whether the icons such as
	 * "BONGA SERVICES,M-FLEX,DATA AND SMS PLANS,MY SMS SERVICES,SAMBAZA SERVICES,SKIZA SERVICES,SERVICES,ROAMING SERVICES AND OKOA SERVICES "
	 *  and Title should be "SERVICES"
	 */
	@Test
	public void SERVICES_TC_001() throws InterruptedException, IOException {
		
		wait.until(ExpectedConditions.elementToBeClickable(dpHomeObject.menu_expand)).click();
		wait.until(ExpectedConditions.elementToBeClickable(menudrawerObject.services_click)).click();
		String expected_confirmation_title = "SERVICES";
		String actual_confirmation_title = menuservicesObject.services_title.getText();
		Assert.assertEquals(actual_confirmation_title, expected_confirmation_title);
		Assert.assertEquals(true, menuservicesObject.bongaservice_click.isDisplayed(),"Bonga Services icon is not displayed");
		Assert.assertEquals(true, menuservicesObject.flexServices_click.isDisplayed(),"Flex icon is not displayed");
		Assert.assertEquals(true, menuservicesObject.datansmsplans_click.isDisplayed(),"Data and SMS Plans icon is not displayed");
		Assert.assertEquals(true, menuservicesObject.mysmsservices_click.isDisplayed(),"My SMS Services icon is not displayed");
		Assert.assertEquals(true, menuservicesObject.sambazaservices_click.isDisplayed(),"Sambaza Services icon is not displayed");
		Assert.assertEquals(true, menuservicesObject.skizaservices_click.isDisplayed(),"Skiza Services icon is not displayed");
		Assert.assertEquals(true, menuservicesObject.roamingservices_click.isDisplayed(),"Roaming Services icon is not displayed");
		Assert.assertEquals(true, menuservicesObject.okoaservices_click.isDisplayed(),"Okoa Services icon is not displayed");
		
	}

}
