package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.dphome.MpesaHomePage;
import com.safaricom.pages.dphome.dpHomePage;
import com.safaricom.pages.dphome.sfcHomePage;
import com.safaricom.pages.mpesa.MPesaGlobalPage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class MpesaGlobalTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public MpesaHomePage mpesahomepageObject = new MpesaHomePage(driver);
	public MPesaGlobalPage mpesaGlobalObject = new MPesaGlobalPage(driver);
	public dpHomePage dpHomeObject=new dpHomePage(driver);
	public sfcHomePage sfcHomePageObject = new sfcHomePage(driver);
	/*
	 * Verify whether user is able to Opt in to M-Pesa Global and the title should
	 * be "M-PESA GLOBAL"
	 */

	@Test
	public void MPESAGLOBAL_TC_001() throws InterruptedException, IOException {

		wait.until(ExpectedConditions.elementToBeClickable(dpHomeObject.mpesa_click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(mpesahomepageObject.mpesaglobalClick)).click();
		Thread.sleep(5000);
		String expectedtitle = "M-PESA GLOBAL";
		String mpesaglobal_title = mpesaGlobalObject.mpesaglobal_title.getText();
		MobileElement region = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().className(\"android.widget.ScrollView\")).scrollIntoView("
				+ "new UiSelector().text(\"Enter Region (Min 3 char)\"))");
		region.sendKeys("India");
		//wait.until(ExpectedConditions.elementToBeClickable(mpesaGlobalObject.region)).sendKeys("India");
		wait.until(ExpectedConditions.elementToBeClickable(mpesaGlobalObject.termsandconditioncheckbox_Click)).click();
		wait.until(ExpectedConditions.elementToBeClickable(mpesaGlobalObject.mpesaglobaloptin_continue)).click();
		Assert.assertEquals(mpesaglobal_title, expectedtitle);
	
	}

}
